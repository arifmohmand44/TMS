
<ul class="sidebar-menu">
    <li class="header">Main Navigation</li>
    <li class="treeview <?php echo ($this->router->fetch_method() === "index")?'active':'' ?>">
        <a href="<?php echo base_url();?>Admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
    </li>
    <li class="treeview <?php echo ($this->router->fetch_method() === "users")?'active':'' ?>">
        <a href="<?php echo base_url();?>users">
            <i class="fa fa-users"></i> <span>Users</span></a>
    </li>
    <li class="treeview <?php echo ($this->router->fetch_method() === "system")?'active':'' ?>">
        <a href="#">
            <i class="fa fa-cogs"></i> <span>System Configuration</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li <?php echo ($this->router->fetch_method() === "system")?'class="active"':'' ?>><a href="<?php echo base_url('sitting'); ?>"><i class="fa fa-circle-o"></i> System</a></li>
            <li <?php echo ($this->router->fetch_method() === "backupDB")?'class="active"':'' ?>>
                <a href="<?php echo base_url('Admin/backupDB'); ?>"><i class="fa fa-circle-o"></i> System Back-Up  </a>
            </li>
        </ul>
    </li>
</ul>