<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            System
            <small>Configuration</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">System</a></li>
            <li class="active">Configuration</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <i class="fa  fa-television"></i>
                        <h3 class="box-title">System Configuration</h3>

                    </div>
                    <div class="box-body">
                        <form id="systemConfigurationForm" method="post">
                            <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Site Name</label>
                                    <input type="text" class="form-control" name="SiteName" id="addSiteName" value="<?=isset($systemConfiguration['SiteName'])?$systemConfiguration['SiteName']:""?>" placeholder="Site Name">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" name="CompanyPhone" id="CompanyPhone" value="<?=isset($systemConfiguration['CompanyPhone'])?$systemConfiguration['CompanyPhone']:""?>" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="addEmail">Company Email</label>
                                    <input type="email" class="form-control" name="CompanyEmail" id="CompanyEmail" placeholder="Company Email" value="<?=isset($systemConfiguration['CompanyEmail'])?$systemConfiguration['CompanyEmail']:""?>">
                                </div>
                            </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" name="CompanyName" id="CompanyName"
                                               value="<?= isset($systemConfiguration['CompanyName']) ? $systemConfiguration['CompanyName'] : "" ?>"
                                               placeholder="Company Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <label for="addAddress">Address</label>
                                        <textarea class="textarea" placeholder="Address" name="CompanyAddress" id="addAddress"
                                                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= isset($systemConfiguration['CompanyAddress']) ? $systemConfiguration['CompanyAddress'] : "" ?></textarea>
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php
//This Section footerScripts Should Execute In Footer/End of the Page.
$this->footerScripts = sprintf('
<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="'.base_url().'assets/plugins/fastclick/fastclick.min.js"></script>
<script src="'.base_url().'assets/plugins/select2/select2.full.min.js"></script>
<script src="'.base_url().'assets/dist/js/app.min.js"></script>

 <!-- InputMask -->
 <script src="'.base_url().'assets/plugins/input-mask/jquery.inputmask.js"></script>
 <script src="'.base_url().'assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
 <script src="'.base_url().'assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

');


$this->footerScripts .= sprintf('
    <script type="text/javascript">
    $(function () {


    //Input mask
    $("#addPatientName").inputmask({

        mask:"aaaaa-999999"
    });
    //End Input Maks

    //Update on Click Event of Update Button.
            var form = $("#systemConfigurationForm");
            form.find("input, textarea").on("change",function(e){
                var inputName = $(this).attr("name");
                var inputValue = $(this).val();
                $.ajax({
                      url: "'.base_url().'Admin/system/update",
                      type:"POST",
                      data: {name:inputName, value:inputValue},
                      success:function(output){
                        var data = output.split("::");
                        ArifMohmand.notification(data[1],data[2]);
                      }
                });

            });
    });
    </script>
');

?>
