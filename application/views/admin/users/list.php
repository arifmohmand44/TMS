<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Users</a></li>
            <li class="active">List</li>
        </ol>
    </section>



    <!-- Main content -->
    <section class="content">
        <div class="col-md-3 pull-right" style="padding-right: 0px;">
            <a href="<?php echo base_url(); ?>add-users" type="button" class="btn btn-block btn-primary pull-right">New Users</a>
        </div>

        <br /><br />
        <!--Filters Box-->
        <div class="box  box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Filter By</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="groupSelector">User Group</label>
                            <select class="form-control select2"  style="width: 100%;" tabindex="4" id="groupSelector" name="groupSelector">

                            </select>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control select2" id="ActiveSelect" name="ActiveSelect" style="width: 100%;">

                            </select>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->


                </div>
            </div>
        </div>
        <!--        End Of Filters Box-->


        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Users List</h3>
                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" placeholder="SearchHere..." id="usersSearchBox" class="form-control input-sm">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="usersList" class="table table-bordered table-hover responsive">
                            <thead>
                            <tr>
                                <th class="mobile tablet desktop">UserID</th>
                                <th class=" tablet desktop">Full Name</th>
                                <th class="  desktop">User Name</th>
                                <th class=" tablet desktop">User Group</th>
                                <th class=" tablet desktop">Status</th>
                                <th class="  desktop">Created Date</th>
                                <th class="  desktop">Created By</th>
                                <th class="  desktop">Updated By</th>
                                <th class="mobile-l tablet desktop">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<div class="modal deleteUser-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="fa fa-lock"></span> Warning</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="DeleteHiddenID">
                <div class="form-group">
                    <label>Are You sure want to <span class="text-red">De-Activate</span> the user login.</label>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary pull-right" id="YesDelete" >Yes</button>
                &nbsp; &nbsp;
                <button type="button" class="btn btn-default " data-dismiss="modal" style="margin-right: 10px;">No</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal enableUser-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="fa fa-unlock"></span> Warning</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="EnableHiddenID">
                <div class="form-group">
                    <label>Are You sure want to <span class="text-green">Activate</span> the user login.</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" id="YesActivate" >Yes</button>
                &nbsp; &nbsp;
                <button type="button" class="btn btn-default " data-dismiss="modal" style="margin-right: 10px;">No</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php
//This Section headerStyles Should Execute In Header/Top head Section of the Page.

 $this->headerStyles = sprintf('
    <link rel="stylesheet" href="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.css">
');

?>


<?php
//This Section footerScripts Should Execute In Footer/End of the Page.
 $this->footerScripts = sprintf('
<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="'.base_url().'assets/plugins/datatables/dataTables.responsive.js"></script>
<script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="'.base_url().'assets/plugins/fastclick/fastclick.min.js"></script>
<script src="'.base_url().'assets/plugins/select2/select2.full.min.js"></script>
<script src="'.base_url().'assets/dist/js/app.min.js"></script>

');


$this->footerScripts .= sprintf('

<script type="text/javascript">
$(document).ready(function(e){
        //// Need To Work ON New Way Of DataTables..
    oTable ="";
     //Initialize Select2 Elements
        $(".select2").select2();
    var usersTableSelector = $("#usersList");
    var url_DT = "'.base_url().'Admin/users/list";
    var aoColumns_DT = [
        /* ID */ {
    "mData": "ID",
            "bVisible": true,
            "bSortable": true,
            "bSearchable": true
        },
        /* Username  */ {
    "mData" : "UName"
        },
        /* Email */ {
    "mData" : "Email"
        },
         /* Group Name */ {
    "mData" : "GName"
        },
         /*  Status */ {
    "mData" : "Status"
        },
        /* CreatedDate */ {
    "mData" : "DateCreated"
        },
        /* CreatedBy */ {
    "mData" : "CBy"
        },
         /* CUpdatedBy */ {
    "mData" : "UBy"
        },
        /* Action Buttons */ {
    "mData" : "ViewEditActionButtons"
        }
    ];
    var HiddenColumnID_DT = "ID";
    var sDom_DT = \'<"H"r>t<"F"<"row"<"col-lg-6 col-xs-12" i> <"col-lg-6 col-xs-12" p>>>\';
    commonDataTables(usersTableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
      new $.fn.dataTable.Responsive( oTable, {
        details: true

        });

        removeWidth(oTable);
    //Little Code For Search Box
        $("#usersSearchBox").keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
        var UserGroupSelector = $("#groupSelector");
        var minInputLength = 0;
        var placeholder = "User Group";
        var UserGroupSelectorURL = "' . base_url() . 'Admin/select_group";
        commonSelect2(UserGroupSelector,UserGroupSelectorURL,minInputLength,placeholder);

        //status  Selector availabilitySelect
        var StatusSelector = $("#ActiveSelect");
        var minInputLength = 0;
        var placeholder = "Status";
        var StatusSelectorURL = "' . base_url() . 'Admin/select_active";
        commonSelect2(StatusSelector,StatusSelectorURL,minInputLength,placeholder);

        //Some Action To Perform When Modal for Deactivating a user Is Shown.
        $(".deleteUser-modal").on("shown.bs.modal", function (e) {
            var button = $(e.relatedTarget); // Button that triggered the modal
            var ID = button.parents("tr").attr("data-id");
            var modal = $(this);
            modal.find("input#DeleteHiddenID").val(ID);
        });

        //Some Action To Perform When Modal for Activating a User Is Shown.
        $(".enableUser-modal").on("shown.bs.modal", function (e) {
            var button = $(e.relatedTarget); // Button that triggered the modal
            var ID = button.parents("tr").attr("data-id");
            var modal = $(this);
            modal.find("input#EnableHiddenID").val(ID);
        });



        // to delete a record from Beds table
        $("#YesDelete").on("click", function (e) {
            var ID = $("#DeleteHiddenID").val();
            var Table="tms_users";
            var Column="IsActive";
            var PrimaryKey="UserID";
            var postData={ID:ID,Table:Table,Column:Column,PrimaryKey:PrimaryKey};
            $.ajax({
                url: "'.base_url().'Admin/users/disabled",
                type: "post",
                data: postData,
            success:function(output){
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        ArifMohmand.notification(data[1],data[2]);
                     $(".deleteUser-modal").modal("hide");
                     oTable.fnDraw();
                    }else if(data[0] === "FAIL"){
                        ArifMohmand.notification(data[1],data[2]);
                    }
                }
               });
        });

        // to Activate a record/User login
        $("#YesActivate").on("click", function (e) {
            var ID = $("#EnableHiddenID").val();
            var Table="tms_users";
            var Column="IsActive";
            var PrimaryKey="UserID";
            var postData={ID:ID,Table:Table,Column:Column,PrimaryKey:PrimaryKey};
            $.ajax({
                url: "'.base_url().'Admin/users/enable",
                type: "post",
                data: postData,
            success:function(output){
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        ArifMohmand.notification(data[1],data[2]);
                     $(".enableUser-modal").modal("hide");
                     oTable.fnDraw();
                    }else if(data[0] === "FAIL"){
                        ArifMohmand.notification(data[1],data[2]);
                    }
                }
               });
        });



          $(".select2").on("change",function(e){
            oTable.fnDestroy();
            var selectedValue = $(this).val();
            var selectedBox = $(this).attr("name");
            var filters = "";
            if(selectedBox === "groupSelector"){
                 var Active = $("#ActiveSelect").val();
                 filters = \'aoData.push({"name":"\'+selectedBox+\'","value":\'+selectedValue+\'});aoData.push({"name":"ActiveSelect","value":\'+Active+\'});\';
                 }
            else if(selectedBox === "ActiveSelect"){
                var group = $("#groupSelector").val();
                if(!selectedValue){
                    selectedValue = 0;
                }
                filters = \'aoData.push({"name":"\'+selectedBox+\'","value":\'+selectedValue+\'}); aoData.push({"name":"groupSelector","value":\'+group+\'});\';
                }
            commonDataTables(usersTableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,"","",filters);


}); // End select2 change Event function

});

</script>

');
?>

<?php
//Get the Flash Data
$alertMsg = $this->session->flashdata('alertMsg');
//Code Page Alert Messages If Any.
if(isset($alertMsg) && !empty($alertMsg)){
    $this->footerScripts .= sprintf('

    <script type="text/javascript">
    $(document).ready(function(){
    var message = \''.$alertMsg.'\';
        message = message.split("::");
        ArifMohmand.notification(message[0],message[1],message[2]);
    });
    </script>

    ');
}
?>
