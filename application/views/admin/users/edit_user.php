<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 9/19/2015
 * Time: 3:23 PM
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?=$this->lang->line('headEdit')?>
            <small><?=$this->lang->line('headUsers')?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> <?=$this->lang->line('headHome')?></a></li>
            <li><a href="<?=base_url()?>Admin/users"><?=$this->lang->line('headUsers')?></a></li>
            <li class="active"><?=$this->lang->line('headEdit')?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="<?=base_url();?>Admin/users/edit/<?=$this->uri->segment(4);?>" method="post" id="admissionForm">
            <!-- SELECT2 EXAMPLE -->
            <div class="box  box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=$this->lang->line('headUserPersonalInfo')?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="userName"><?=$this->lang->line('lblFullName')?></label>
                                <input class="form-control" type="text" value="<?= isset($UserDetails->Username)?$UserDetails->Username:""?>" required="required" name="userName" id="userName" placeholder="<?=$this->lang->line('plcUserName')?>" tabindex="1">
                            </div><!-- /.form-group -->
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="userEmail"><?=$this->lang->line('lblUserName')?></label>
                                <input class="form-control" type="text" required="required" value="<?= isset($UserDetails->Email)?$UserDetails->Email:""?>" name="userEmail" id="userEmail" placeholder="<?=$this->lang->line('plcEmailAddress')?>" tabindex="2">
                            </div><!-- /.form-group -->

                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="userPassword"><?=$this->lang->line('lblUserPassword')?></label>
                                <input class="form-control" type="text" value="<?= isset($UserDetails->Password)?$UserDetails->Password:""?>" required="required" name="userPassword" id="userPassword" placeholder="<?=$this->lang->line('plcUserPassword')?>" tabindex="1">
                            </div><!-- /.form-group -->
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="groupSelector"><?=$this->lang->line('lblGroup')?></label>
                                <select class="form-control select2" id="groupSelector" name="groupSelector" style="width: 100%;">
                                    <?php echo (isset($UserDetails->GroupID))?"<option value='".$UserDetails->GroupID."' selected='selected'>".$UserDetails->GroupTitle."</option>":'';?>
                                </select>
                            </div><!-- /.form-group -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row" id="hiddenSpecialityDiv" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="specialitySelector"><?=$this->lang->line('lblSpeciality')?></label>
                                <select class="form-control select2" id="specialitySelector" name="specialitySelector" style="width: 100%;" tabindex="8">
                                    <?php echo (isset($SpecialityDetails->ID))?"<option value='".$SpecialityDetails->ID."' selected='selected'>".$SpecialityDetails->Speciality."</option>":'';?>
                                </select>
                            </div><!-- /.form-group -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-3 pull-right">
                            <button type="submit" class="btn btn-block btn-primary btn-flat" id="updateUser" tabindex="16"><?=$this->lang->line('btnUpdateUser')?></button>
                        </div>
                        <!-- iCheck -->
                    </div><!-- /.col (right) -->
                </div>
            </div><!-- /.box-body -->

</form>
</section><!-- /.content -->

</div><!-- /.box -->

<?php
//This Section footerScripts Should Execute In Footer/End of the Page.
$this->footerScripts = sprintf('
<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="'.base_url().'assets/plugins/fastclick/fastclick.min.js"></script>
<script src="'.base_url().'assets/plugins/select2/select2.full.min.js"></script>
<script src="'.base_url().'assets/dist/js/app.min.js"></script>

');


$this->footerScripts .= sprintf('

<script type="text/javascript">
$(document).ready(function(e){
        //// Need To Work ON New Way Of DataTables..
        var userGroupID = '.(isset($UserDetails->GroupID)?$UserDetails->GroupID:"").';

    //Selectors For Filters.
        //consultant  Selector
        var groupSelector = $("#groupSelector");
        var minInputLength = 0;
        var placeholder = "'.$this->lang->line('plcSelectGroup').'";
        var groupSelectorURL = "' . base_url() . 'Admin/select_group";
        commonSelect2(groupSelector,groupSelectorURL,minInputLength,placeholder);
        //Speciality  Selector
        var specialitySelector = $("#specialitySelector");
        var minInputLength = 0;
        var placeholder = "'.$this->lang->line('plcSelectspeciality').'";
        var groupSelectorURL = "' . base_url() . 'Admin/select_speciality";
        commonSelect2(specialitySelector,groupSelectorURL,minInputLength,placeholder);

        //Just a piece of code to display speiciality if SelectedGroup is Consultant
        var selectedGroupID = groupSelector.val();
        if(selectedGroupID === "5"){
           $("#hiddenSpecialityDiv").show();
        }


        groupSelector.on("change",function(){
            var groupID = $(this).val();
            if(groupID === "5"){
                //Do the Work for Consultant Box
                $("#hiddenSpecialityDiv").show();
            }else{
                $("#hiddenSpecialityDiv").hide();
            }
        });


}); // End select2 change Event function

</script>
');

?>

<?php
//Get the Flash Data
$alertMsg = $this->session->flashdata('alertMsg');
//Code Page Alert Messages If Any.
if(isset($alertMsg) && !empty($alertMsg)){
    $this->footerScripts .= sprintf('

    <script type="text/javascript">
    var message = \''.$alertMsg.'\';
        message = message.split("::");
        Haider.notification(message[0],message[1],message[2]);

    </script>

    ');
}
?>
<script type="text/javascript">

</script>
