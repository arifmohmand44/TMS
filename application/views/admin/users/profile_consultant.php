<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/29/2016
 * Time: 3:47 PM
 */

//  for profile
$profile = array(
    'class' => 'form-horizontal',
    'id' => 'settings'
);
$label = array(
    'class' => 'col-sm-2 control-label'
);
$name = array(
    'id' => 'inputName',
    'class' => 'form-control',
    'placeholder' => 'Name',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($UserProfileDetails->Name) && !empty($UserProfileDetails->Name))?$UserProfileDetails->Name:""
);
$email = array(
    'id' => 'inputEmail',
    'class' => 'form-control',
    'placeholder' => 'Email',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($UserProfileDetails->Email) && !empty($UserProfileDetails->Email))?$UserProfileDetails->Email:""
);
$Username = array(
    'id' => 'inputUsername',
    'class' => 'form-control',
    'name' => 'inputUserName',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($UserProfileDetails->Username) && !empty($UserProfileDetails->Username))?$UserProfileDetails->Username:"",
    'disabled' => 'disabled'
);
$contact = array(
    'id' => 'inputContact',
    'class' => 'form-control',
    'name' => 'inputContact',
    'placeholder' => 'Contact',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($UserProfileDetails->Contact) && !empty($UserProfileDetails->Contact))?$UserProfileDetails->Contact:""
);
$address = array(
    'id' => 'inputAddress',
    'class' => 'form-control',
    'placeholder' => 'Address',
    'rows' => '5',
    'cols' => '30',
    'style' => 'resize:none',
    'value' => (isset($UserProfileDetails->Address) && !empty($UserProfileDetails->Address))?$UserProfileDetails->Address:""
);

// for security
$security = array(
    'class' => 'form-horizontal',
    'id' => 'security'
);
$oldPassword = array(
    'id' => 'currentPassword',
    'class' => 'form-control',
    'name' => 'inputPassword',
    'placeholder' => 'Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => '',
    'type' => 'Password'
);
$newPassword = array(
    'id' => 'newPassword',
    'class' => 'form-control',
    'name' => 'newPassword',
    'placeholder' => 'New Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => ''
);
$confirmPassword = array(
    'id' => 'confirmPassword',
    'class' => 'form-control',
    'name' => 'confirmPassword',
    'placeholder' => 'Confirm Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => ''
);


?>
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Profile
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User profile</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle"
                                 src="<?php echo base_url(); ?>assets/dist/img/user4-128x128.jpg"
                                 alt="User profile picture">

                            <h3 class="profile-username text-center"><?php echo $this->data['UserProfile']->FullName; ?></h3>

                            <p class="text-muted text-center"><?php echo $this->data['Role']; ?></p>

                            <div class="box-footer">
                                <button name="inputSubmit" type="button" id="UpdateProfileBtn"
                                        class="btn btn-success btn-block">Update Profile
                                </button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">

                            <li class="active"><a href="#profile" data-toggle="tab" aria-expanded="true">Profile</a>
                            </li>
                            <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Security</a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">

                                <div class="form-group">
                                    <?php echo form_open('', $profile); ?>
                                    <div class="form-group">
                                        <?php echo form_label('Username', 'inputUsername', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($Username); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Name', 'inputName', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($name); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Email', 'inputEmail', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($email); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Contact', 'inputContact', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($contact); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Address', 'inputAddress', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_textarea($address); ?>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <!-- The timeline -->
                                <?php echo form_open('', $security); ?>
                                <div class="form-group">

                                    <?php echo form_label('Old Password', 'oldPassword', $label); ?>
                                    <div class="col-sm-10">
                                        <?php echo form_input($oldPassword); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('New Password', 'newPassword', $label); ?>

                                    <div class="col-sm-10">
                                        <?php echo form_input($newPassword); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('Confirm Password', 'confirmPassword', $label); ?>
                                    <div class="col-sm-10">
                                        <?php echo form_input($confirmPassword); ?>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

<?php
//This Section footerScripts Should Execute In Footer/End of the Page.
$this->footerScripts = sprintf('
    <script>
       $(document).ready(function(e){


            $("#UpdateProfileBtn").on("click",function(e){
                //Now We Need To Update Database On Ajax Submit.
                  e.preventDefault();

                 var data = {
                     Name: $("#inputName").val(),
                     Email: $("#inputEmail").val(),
                     Contact: $("#inputContact").val(),
                     Address: $("#inputAddress").val(),
                     CurrentPass: $("#currentPassword").val(),
                     NewPass: $("#newPassword").val(),
                     ConfirmNewPass: $("#confirmPassword").val()
                 };

                var url = "' . base_url() . 'Admin/UpdateProfile";
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(output){
                    try{
                     var data = output.split("::");
                        Haider.notification(data[1],data[2]);
                    }
                    catch(ex){
                        Haider.notification("Some Error Occurred - "+ex,"error");
                    }

                    }
            });

            });
       });
    </script>
');
?>