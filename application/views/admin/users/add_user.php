<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ArifMohmand
 * Date: 9/19/2015
 * Time: 3:23 PM
 */?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New
            <small>User</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Users</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <form action="<?=base_url();?>Admin/users/add" method="post" id="admissionForm">
        <!-- SELECT2 EXAMPLE -->
        <div class="box  box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">User Personal Info</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fullName">Full Name</label>
                            <input class="form-control" type="text" required="required" name="fullName" id="fullName" placeholder="Full Name" tabindex="1">
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="userEmail">Email Address</label>
                            <input class="form-control" type="email" required="required" name="userEmail" id="userEmail" placeholder="Email Address" tabindex="4">
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="userPassword">Password</label>
                            <input class="form-control" type="password" required="required" name="userPassword" id="userPassword" placeholder="Password" tabindex="5">
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password</label>
                            <input class="form-control" type="password" required="required" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" tabindex="6">
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div>
            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="groupSelector">User Group</label>
                        <select class="form-control select2" id="groupSelector" name="groupSelector" style="width: 100%;" tabindex="7"></select>
                    </div><!-- /.form-group -->
                </div><!-- /.col -->
            </div>
                <div class="row">
                    <div class="col-md-3 pull-right">
                        <button type="submit" class="btn btn-block btn-primary pull-right" id="addUser" tabindex="10">Add User</button>
                    </div>
                    <!-- iCheck -->
                </div><!-- /.col (right) -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <!-- /.row -->
    </form>
    </section><!-- /.content -->
</div>
<?php
//This Section footerScripts Should Execute In Footer/End of the Page.
$this->footerScripts = sprintf('
<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="'.base_url().'assets/plugins/fastclick/fastclick.min.js"></script>
<script src="'.base_url().'assets/plugins/select2/select2.full.min.js"></script>
<script src="'.base_url().'assets/dist/js/app.min.js"></script>
');

$this->footerScripts .= sprintf('
<script type="text/javascript">
$(document).ready(function(e){

     // to restrict numaric value in admit patient name and father name
$("#fullName,#fatherName").keydown(function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
     } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 9) || (key == 16)|| (key == 32)  || (key == 0) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 122))) {
            e.preventDefault();
        }
     }
    });
    $("#userName").keydown(function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
     } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 9) || (key == 16)|| (key == 32)  || (key == 0) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 122))) {
            e.preventDefault();
        }
     }
    });
    //// Need To Work ON New Way Of DataTables..
    //Selectors For Filters.
        //consultant  Selector
        var groupSelector = $("#groupSelector");
        var minInputLength = 0;
        var placeholder = "Select User Group";
        var groupSelectorURL = "' . base_url() . 'Admin/select_groups";
        commonSelect2(groupSelector,groupSelectorURL,minInputLength,placeholder);

        $("body").on("change","#userEmail",function(){
                  var userEmail = $("#userEmail").val();
               $.ajax({
                      url: "'.base_url().'Admin/checkEmail",
                      type:"POST",
                      data: {userEmail:userEmail},
                      success:function(output){
                        var data = output.split("::");
                        ArifMohmand.notification(data[1],data[2]);
                      }
                });
        });
        $("#addUser").on("click",function(e){
            e.preventDefault();

            var fullName = $("#fullName").val();
            var userEmail = $("#userEmail").val();
            var userPassword = $("#userPassword").val();
            var confirmPassword = $("#confirmPassword").val();

            var groupID = $("#groupSelector").val();

             if(fullName.length === 0){
                    ArifMohmand.notification("Please Enter  Full Name","error");
                    return false;
                }
                if(userEmail.length === 0){
                    ArifMohmand.notification("Please Enter  Email Address","error");
                    return false;
                }
                if(userPassword.length === 0){
                    ArifMohmand.notification("Please Enter  Password","error");
                    return false;
                }
                if(confirmPassword.length === 0){
                    ArifMohmand.notification("Please Enter  Confirm Password","error");
                    return false;
                }
                if(userPassword !== confirmPassword){
                    ArifMohmand.notification("Passwords Do Not Match","error");
                    return false;
                }
                if(groupID ==null || groupID.length === 0){
                    ArifMohmand.notification("Please Select User Group","error");
                    return false;
                }
            if(groupID == 5){
                var specialityID = $("#specialitySelector").val();
                var doctorRoomID = $("#doctorRoomSelector").val();
                if(specialityID == null || specialityID.length === 0){
                    ArifMohmand.notification("Please Select Consultant Speciality From the DropDown","error");
                    return false;
                }
                else if(doctorRoomID == null || doctorRoomID.length === 0){
                    ArifMohmand.notification("Please Select Doctor Room From the DropDown","error");
                    return false;
                }else{
                    $("#admissionForm").submit();
                    return true;
                }
            }
            else{
            $("#admissionForm").submit();
                    return true;
            }
        });

}); // End select2 change Event function
</script>
');
?>

<?php
//Get the Flash Data
$alertMsg = $this->session->flashdata('alertMsg');
//Code Page Alert Messages If Any.
if(isset($alertMsg) && !empty($alertMsg)){
    $this->footerScripts .= sprintf('
    <script type="text/javascript">
    var message = \''.$alertMsg.'\';
        message = message.split("::");
        ArifMohmand.notification(message[0],message[1],message[2]);
    </script>

    ');
}
?>
<script type="text/javascript"></script>
