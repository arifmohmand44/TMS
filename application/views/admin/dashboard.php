<style>
    .inner{
        color: white;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <!-- <small>Version 100.2</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="<?=base_url()?>users" style="color: white">
                <div class="small-box bg-green-gradient">
                    <div class="inner">
                        <h3 style="opacity: .0;">53<sup style="font-size: 20px">%</sup></h3>
                       <p>Users</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-plus"></i>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                </div>
               </a>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="<?=base_url()?>sitting" style="color: white">
                    <div class="small-box blackbackground">
                        <div class="inner">
                            <h3 style="opacity: .0;">53<sup style="font-size: 20px">%</sup></h3>
                            <p>Setting Configuration</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-cogs"></i>
                        </div>
                        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>
        </div><!-- /.row -->


        <!-- Main row -->
        <!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php

$this->footerScripts = sprintf('
<script src="'.base_url().'assets/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="'.base_url().'assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="'.base_url().'assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="'.base_url().'assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
');
?>
