<?php
/**
 * Created by PhpStorm.
 * User: COD3R
 * Date: 9/19/2015
 * Time: 3:08 PM
 */
//  for profile
$profile = array(
    'class' => 'form-horizontal',
    'id' => 'settings'
);
$label = array(
    'class' => 'col-sm-2 control-label'
);
$name = array(
    'id' => 'inputName',
    'class' => 'form-control',
    'placeholder' => 'Name',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($AdminProfileDetails->Name) && !empty($AdminProfileDetails->Name))?$AdminProfileDetails->Name:""
);
$email = array(
    'id' => 'inputEmail',
    'class' => 'form-control',
    'placeholder' => 'Email',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($AdminProfileDetails->Email) && !empty($AdminProfileDetails->Email))?$AdminProfileDetails->Email:""
);
$Username = array(
    'id' => 'inputUsername',
    'class' => 'form-control',
    'name' => 'inputUserName',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($AdminProfileDetails->Username) && !empty($AdminProfileDetails->Username))?$AdminProfileDetails->Username:"",
    'disabled' => 'disabled'
);
$contact = array(
    'id' => 'inputContact',
    'class' => 'form-control',
    'name' => 'inputContact',
    'placeholder' => 'Contact',
    'size' => '30',
    'maxlength' => '30',
    'value' => (isset($AdminProfileDetails->Contact) && !empty($AdminProfileDetails->Contact))?$AdminProfileDetails->Contact:""
);
$address = array(
    'id' => 'inputAddress',
    'class' => 'form-control',
    'placeholder' => 'Address',
    'rows' => '5',
    'cols' => '30',
    'style' => 'resize:none',
    'value' => (isset($AdminProfileDetails->Address) && !empty($AdminProfileDetails->Address))?$AdminProfileDetails->Address:""
);

// for security
$security = array(
    'class' => 'form-horizontal',
    'id' => 'security'
);
$oldPassword = array(
    'id' => 'currentPassword',
    'class' => 'form-control',
    'name' => 'inputPassword',
    'placeholder' => 'Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => '',
    'type' => 'Password'
);
$newPassword = array(
    'id' => 'newPassword',
    'class' => 'form-control',
    'name' => 'newPassword',
    'placeholder' => 'New Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => ''
);
$confirmPassword = array(
    'id' => 'confirmPassword',
    'class' => 'form-control',
    'name' => 'confirmPassword',
    'placeholder' => 'Confirm Password',
    'size' => '30',
    'maxlength' => '30',
    'value' => ''
);


?>
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Profile
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User profile</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">
                     <style>
                         #preview-avatar-profile img{
                             width: auto;
                         }
                         .modal-body #photoimg{
                             margin: 0 auto;
                         }
                         #profileImgDiv > a#a{
                            background: black none repeat scroll 0 0;
                            color: white;
                            display: block;
                            margin-top: -30px;
                            opacity: 0;
                            padding: 5%;
                            transition: opacity 0.13s ease-out 0s;
                            width: 100%;
                        }

                         #profileImgDiv > a#a:hover{
                            opacity: 0.5;
                        }
                        #profileImgDiv:hover{
                            background: transparent;
                            transition: opacity 0.13s ease-out 0s;
                            margin: 0 auto;
                            border: 3px solid #4ba14f;
                        }

                        #profileImgDiv {
                            background: transparent none repeat scroll 0 0;
                            border: 3px solid #d2d6de;
                            display: block;
                            float: none;
                            height: auto;
                            margin: 0 auto;
                            max-width: 100%;
                            padding: 3px;
                            transition: opacity 0.13s ease-out 0s;
                            vertical-align: middle;
                        }
                         .profileImg{
                             float: none !important;
                             margin: 0 auto;
                             text-align: center;
                             width: 100%;
                         }

                        @media (max-width: 768px) { /*xm*/
                            #profileImgDiv {
                               width:100px;
                            }
                        }
                     </style>
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <div class="row profileImg">
                               <div id="profileImgDiv" class="col-md-8 col-sm-2 col-xs-3" style="text-align: center;">
                                   <img class="img-responsive "
                                        src="<?=getUserProfileImage($AdminProfileDetails->UserID)?>"
                                        alt="User profile picture">
                                   <a id="a" href="#" data-target=".Editimg-modal" data-toggle="modal">Update</a>
                               </div>
                            </div>
                            <h3 class="profile-username text-center"><?php echo $this->data['UserProfile']->FullName; ?></h3>

                            <p class="text-muted text-center"><?php echo $this->data['Role']; ?></p>

                            <div class="box-footer">
                                <button name="inputSubmit" type="button" id="UpdateProfileBtn"
                                        class="btn btn-success btn-block">Update Profile
                                </button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">

                            <li class="active"><a href="#profile" data-toggle="tab" aria-expanded="true">Profile</a>
                            </li>
                            <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Security</a>
                            </li>
                     </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">

                                <div class="form-group">
                                    <?php echo form_open('', $profile); ?>
<!--                                    <div class="form-group">-->
<!--                                        --><?php //echo form_label('Username', 'inputUsername', $label); ?>
<!--                                        <div class="col-sm-10">-->
<!--                                            --><?php //echo form_input($Username); ?>
<!--                                        </div>-->
<!--                                    </div>-->
                                    <div class="form-group">
                                        <?php echo form_label('Full Name', 'inputName', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($name); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('UserName (Email)', 'inputEmail', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($email); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Contact', 'inputContact', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_input($contact); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label('Address', 'inputAddress', $label); ?>
                                        <div class="col-sm-10">
                                            <?php echo form_textarea($address); ?>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <!-- The timeline -->
                                <?php echo form_open('', $security); ?>
                                <div class="form-group">

                                    <?php echo form_label('Old Password', 'oldPassword', $label); ?>
                                    <div class="col-sm-10">
                                        <?php echo form_input($oldPassword); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('New Password', 'newPassword', $label); ?>

                                    <div class="col-sm-10">
                                        <?php echo form_input($newPassword); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('Confirm Password', 'confirmPassword', $label); ?>
                                    <div class="col-sm-10">
                                        <?php echo form_input($confirmPassword); ?>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <div class="modal Editimg-modal" id="updateAvatarModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">

                            <div id='preview-avatar-profile'>

                            </div>
                            <div id="thumbs" style="padding:1%; width:40%;"></div>
                            <form id="cropimage" method="post" enctype="multipart/form-data" action="<?=base_url()?>Admin/saveImageTemp">
                            <input type="hidden" name="hdn-profile-id" id="hdn-profile-id" value="1" />
                            <input type="hidden" name="hdn-x1-axis" id="hdn-x1-axis" value="" />
                            <input type="hidden" name="hdn-y1-axis" id="hdn-y1-axis" value="" />
                            <input type="hidden" name="hdn-x2-axis" value="" id="hdn-x2-axis" />
                            <input type="hidden" name="hdn-y2-axis" value="" id="hdn-y2-axis" />
                            <input type="hidden" name="hdn-thumb-width" id="hdn-thumb-width" value="" />
                            <input type="hidden" name="hdn-thumb-height" id="hdn-thumb-height" value="" />
                            <input type="hidden" name="action" value="" id="action" />
                            <input type="hidden" name="image_name" value="" id="image_name" />
                            <input type="file" class="btn btn-default" id="photoimg" name="photoimg" accept="image/*">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mright" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateProfileImage">Save changes</button>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<!--for image view-->
    <script>
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
<?php

$this->footerScripts = sprintf('
       <script src="'.base_url().'assets/plugins/imageareaselect/jquery.imgareaselect.js"></script>
       <script src="'.base_url().'assets/dist/js/jquery.form.js"></script>
       <script src="' . base_url() . 'assets/dist/js/moment.min.js"></script>
       <script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="'.base_url().'assets/plugins/datatables/dataTables.responsive.js"></script>
        <script src="'.base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="'.base_url().'assets/plugins/fastclick/fastclick.min.js"></script>
        <script src="'.base_url().'assets/plugins/select2/select2.full.min.js"></script>
        <script src="'.base_url().'assets/dist/js/app.min.js"></script>
       <link rel="stylesheet" href="'.base_url().'assets/plugins/imageareaselect/imgareaselect.css">
');

//This Section footerScripts Should Execute In Footer/End of the Page.
$this->footerScripts .= sprintf('
    <script>
       $(document).ready(function(e){

            $("#photoimg").on("change",function(e){
                $("#cropimage").ajaxForm(
		            {
		                target: "#preview-avatar-profile",
		                success:    function(output) {
		                    console.log(output);
                            $("img#photo").imgAreaSelect({
				            aspectRatio: "1:1",
				            onSelectEnd: getSizes
			                });
			            $("#image_name").val($("#photo").attr("file-name"));
			        }
		        }).submit();

            });

	$("#updateProfileImage").on("click", function(e){
    e.preventDefault();
    params = {
    targetUrl: "'.base_url().'Admin/cropAvatarImage",
            action: "save",
            x_axis: jQuery("#hdn-x1-axis").val(),
            y_axis : jQuery("#hdn-y1-axis").val(),
            x2_axis: jQuery("#hdn-x2-axis").val(),
            y2_axis : jQuery("#hdn-y2-axis").val(),
            thumb_width : jQuery("#hdn-thumb-width").val(),
            thumb_height:jQuery("#hdn-thumb-height").val()
        };
        saveCropImage(params);
    });


    function saveCropImage(params) {
    jQuery.ajax({
        url: params["targetUrl"],
        cache: false,
        dataType: "html",
        data: {
            action: params["action"],
            id: jQuery("#hdn-profile-id").val(),
             t: "ajax",
              w1:params["thumb_width"],
              x1:params["x_axis"],
              h1:params["thumb_height"],
              y1:params["y_axis"],
              x2:params["x2_axis"],
              y2:params["y2_axis"],
			image_name :jQuery("#image_name").val()
        },
        type: "Post",
       // async:false,
        success: function (response) {
    $("#updateAvatarModal").modal("hide");
    $(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css("display", "none");

    $("#profileImgDiv img").attr("src", response);
    $("#preview-avatar-profile").html("");
    $("#photoimg").val();
    $("#cropimage")[0].reset();
    $(".imgareaselect-selection").parent().css("display","none");
},
    error: function (xhr, ajaxOptions, thrownError) {
    alert("status Code:" + xhr.status + "Error Message :" + thrownError);
}
    });
    }


    function getSizes(img, obj)
    {
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = obj.width;
        var thumb_height = obj.height;
        if(thumb_width > 0)
            {
                jQuery("#hdn-x1-axis").val(x_axis);
                jQuery("#hdn-y1-axis").val(y_axis);
                jQuery("#hdn-x2-axis").val(x2_axis);
                jQuery("#hdn-y2-axis").val(y2_axis);
                jQuery("#hdn-thumb-width").val(thumb_width);
                jQuery("#hdn-thumb-height").val(thumb_height);
            }
        else
            alert("Please select portion..!");
    }


            $("#UpdateProfileBtn").on("click",function(e){
                //Now We Need To Update Database On Ajax Submit.
                  e.preventDefault();

                 var data = {
                     Name: $("#inputName").val(),
                     Email: $("#inputEmail").val(),
                     Contact: $("#inputContact").val(),
                     Address: $("#inputAddress").val(),
                     CurrentPass: $("#currentPassword").val(),
                     NewPass: $("#newPassword").val(),
                     ConfirmNewPass: $("#confirmPassword").val()
                 };

                var url = "' . base_url() . 'Admin/UpdateProfile";
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(output){
                    try{
                     var data = output.split("::");
                        ArifMohmand.notification(data[1],data[2]);
                    }
                    catch(ex){
                        ArifMohmand.notification("Some Error Occurred - "+ex,"error");
                    }

                    }
            });

            });
       });
    </script>
');
?>