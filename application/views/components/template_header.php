<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TMS <?php echo (isset($PageTitle) && !empty($PageTitle))?"| ".$PageTitle:""; ?></title>
    <link rel="shortcut icon" href="<?=base_url('assets/dist/img/favicon.ico')?>" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <!--for date picker-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/datePickerUI/jquery-ui.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/responsive.bootstrap.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">

    <!--    Custom Styles of the Application-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style.css">


    <!--    imgAreaSelect -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/imgareaselect/css/imgareaselect-default.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>assets/dist/js/html5shiv.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>T</b>MS</span>
            <!-- logo for regular state and mobile devices -->

            <span class="logo-lg size"><?=(isset($CompanyDetails['CompanyName']))&&!empty($CompanyDetails['CompanyName'])?$CompanyDetails['CompanyName']:"Your System Name";?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <?php $this->load->view('components/navbar_top.php',array("UserID" => isset($UserID)?$UserID:0)); ?>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
     <?php $this->load->view('components/side_bars/left_sidebar_main',array("UserID" => isset($UserID)?$UserID:0)); ?>