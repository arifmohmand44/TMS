<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php $this->load->view('components/side_bars/left_userpanel',isset($UserID)?$UserID:0); ?>
        <!-- search form -->
        <?php $this->load->view('components/side_bars/left_search'); ?>
        <!-- /.search form -->
        <?php $Controller = $this->router->fetch_class(); ?>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php $this->load->view(strtolower($Controller).'/navigation'); ?>
    </section>
    <!-- /.sidebar -->
</aside>