<div class="user-panel">
    <div class="pull-left image">
        <img src="<?=getUserProfileImage(isset($UserID)?$UserID:0)?>" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p><?php echo $this->data['UserProfile']->FullName; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
    </div>
</div>