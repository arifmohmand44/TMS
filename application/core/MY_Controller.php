<?php
/**
 * Created by PhpStorm.
 * User: COD3R
 * Date: 9/16/2015
 * Time: 4:19 PM
 */
/**
 * @property Common_model $Common_model It resides all the methods which can be used in most of the controllers.
 * @property Users_Auth $Users_Auth It resides all the methods which can be used in most of the controllers.
 * @property CI_Session $session
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_DB_driver $db
 * @property image_lib $image_lib
 * @property upload $upload
 */
class MY_Controller extends CI_Controller
{
    /**
     Pre Defined User Roles Are Below, Defined in Users_Auth Model.
        1. Admin
        2. Accountant
        3. Laboratorist
        4. Pharmacist
        5. Consultant
     */
    public $footerScripts;
    public $data = array();
    public $language;

    function __construct()
    {
        date_default_timezone_set( 'Asia/Karachi' );
        parent::__construct();

        $language = 'english';
        //Loading Some files which will be commonly used across the site.
        $this->load->helper('my_site');
        $this->load->helper('my_user');
        $this->load->model('Common_model');
        $this->load->model('Users_Auth');

        //First Job is to Check If User is LoggedIn, If not throw user to Login Page.
        $this->Users_Auth->is_logged_in();
        //Set Initial Values Which we might would commonly in our application.
        //DateTime Formatting For MySQL
        $this->data['MySQLDateTimeFormat'] = "Y-m-d H:i:s";
        $this->data['MySQLDateFormat'] = "Y-m-d";
        $this->data['dbCurrentDateTime'] = date($this->data['MySQLDateTimeFormat']);
        $this->data['dbCurrentDate'] = date($this->data['MySQLDateFormat']);

        //DateTime Formatting For Site
        $this->data['SitePHPDateTimeFormat'] = "d-m-Y h:i A";
        $this->data['SitePHPDateFormat'] = "d-m-Y";

        ///Values From User Session
        $this->data['UserID'] = $this->session->userdata('UserID');
        $this->data['Email'] = $this->session->userdata('Email');
        $this->data['Username'] = $this->session->userdata('Username');
        $this->data['UserRoleID'] = $this->session->userdata('UserRoleID');
        $this->data['Role'] = $this->session->userdata('Role');

        //Getting UserProfile Now.
        $this->data['UserProfile'] = getUserProfile($this->data['UserID'], $this->data['Role']);

        //Getting Company/Hospital Details
        $this->data['CompanyDetails'] = getCompanyDetails();

        //Site Other Basic Configuration
        $this->data['DevelopedByCompanyName'] = "SUIT";
        $this->data['DevelopedByCompanyWebsite'] = "www.suit.edu.pk";


        //We Need Custom Error Messages Here with Codes, Rather Than Writing Error Messages Again and Again In Views
        //Messages are Shown In Different Colors for Better UI.
        $this->data['errorMsg'] = array(
            'POST' => "Something went wrong with posted values, Please contact system Administrator for further assistance::error",
            'Insert' => "New record could not be created::error",
            'Update' => "Record could not be updated::error",
            'Delete' => "Record could not be delete::error",
            'Database' => "Something went wrong with the database, Please contact system Administrator For Further Assistance::error"
        );
    }

    function show($viewPath, $viewData = array()){
        if(!isset($viewPath) || empty($viewPath)){
            return false;
        }
        $this->load->view('components/template_header', $viewData);
        $this->load->view($viewPath, $viewData);
        $this->load->view('components/template_footer', $viewData);
    }

    /**
     * Common Select2 Selectors
     */
    public function select_gender(){
        $table = "ml_gender";
        $selectData = "ID, Title AS TEXT";
        $search = $this->input->get('q');
        $where = array('IsActive' => 1);
        if(isset($search) && !empty($search)){
            $field = "Title";
            $Genders = $this->Common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$search);
        }else{
            $Genders = $this->Common_model->select_fields_where($table,$selectData,$where);
        }
        if(empty($Genders)){
            $emptyArray = array(
                array(
                    'ID' => 0,
                    'TEXT' => "No Record Found"
                )
            );
            print json_encode($emptyArray);
            return;
        }
        print json_encode($Genders);
    }

    public function checkEmail(){
        if ($this->input->post()) {
            $Email = $this->input->post('userEmail');
            $UserTable = "tms_users";

            //Before Create New User, We First Need To Query Database to Know if This Email Has Already Been Used Or Not.
            $selectData = array("COUNT(1) AS TotalRecordsFound", false);
            $where = "Email = '" . $Email . "'";
            $result = $this->Common_model->select_fields_where($UserTable, $selectData, $where, TRUE);

            if ($result->TotalRecordsFound > 0) {
                echo "FAIL::Email Already Exist::error";
            }
            else{
                echo "FAIL::Email Is Correct::success";

            }
            return;
        }
    }
    /**
     * End Of Old Code.
     */
    public function select_groups(){
        $table = "tms_users_groups";
        $selectData = "GroupID AS ID, Name AS TEXT";
        $where = array('IsActive'=>1);
        $search = $this->input->get('q');
        if(isset($search) && !empty($search)){
            $field = "GroupID";
            $OtStatus= $this->Common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$search);
        }else{
            $OtStatus = $this->Common_model->select_fields_where($table,$selectData,$where);
        }
        if(empty($OtStatus)){
            $emptyArray = array(
                array(
                    'ID' => 0,
                    'TEXT' => "No Record Found"
                )
            );
            print json_encode($emptyArray);
            return;
        }
        print json_encode($OtStatus);

    }
    public function select_group(){
        $table = "tms_users_groups";
        $selectData = "GroupID AS ID, Name AS TEXT";
        $classID = $this->input->get('dependent');
        $where = array('IsActive' => 1); //This is status for Occupied so to know if Bed is Available
        $search = $this->input->get('q');
        if(isset($search) && !empty($search)){
            $field = "GroupID";
            $rooms = $this->Common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$search);
        }else{
            $rooms = $this->Common_model->select_fields_where($table,$selectData,$where);
        }
        if(empty($rooms)){
            $emptyArray = array(
                array(
                    'ID' => 0,
                    'TEXT' => "No Record Found"
                )
            );
            print json_encode($emptyArray);
            return;
        }

        $Group = json_decode(json_encode($rooms),true);

        $allArray = array(
            'ID' => 0,
            'TEXT' => 'All'
        );
        //Push New Array to the Existing ExpenseArray.
        array_push($Group, $allArray);

        $GroupOrder = reorder_default_filter($Group);
        print json_encode($GroupOrder);

        /*print json_encode($rooms);*/
    }
    public function DeleteRecord(){
        if($this->input->post()){ //If Any Values Posted
            if($this->input->is_ajax_request()){ //If Request Generated From Ajax
                //Getting Posted Values
                $ID = $this->input->post('ID');
                $Table = $this->input->post('Table');
                $Column = $this->input->post('Column');


                $updateData = array(
                    $Column =>'0'
                );
                $updateWhere = array(
                    'ID' => $ID
                );
                $result = $this->Common_model->update($Table,$updateWhere,$updateData);
                if($result === true){
                    echo "OK::".$this->lang->line('notyRecordSuccessfullyDeleted');
                    return;
                }else{
                    if($result['code'] === 0){
                        echo "FAIL::".$this->lang->line('notyRecordNotDeleted');
                    }else{
                        echo "FAIL::";print_r($result); echo "::error";
                    }
                    return;
                }
            }
        }
    }
    public function select_active(){
        $search = $this->input->get('q');
        $customArray = array(
            array(
                'ID' => 0,
                'TEXT' => 'DeActive'
            ),
            array(
                'ID' => 1,
                'TEXT' => 'Active'
            ),
            array(
                'ID' => 2,
                'TEXT' => 'All'
            )
        );

        rsort($customArray);

        if(isset($search) && !empty($search)){
            $filteredArray = array_filter(
                $customArray,
                function ($element) use ($search) {
                    return strpos($element, $search) === false;
                }
            );
            if(empty($filteredArray)){
                $emptyArray = array(
                    array(
                        'ID' => 2,
                        'TEXT' => "No Record Found"
                    )
                );
                print json_encode($emptyArray);
            }
            print json_encode($filteredArray);
            return;
        }else{
            print json_encode($customArray);
            return;
        }
    }
    public function ChangeStatus(){
        if($this->input->post()){ //If Any Values Posted
            if($this->input->is_ajax_request()){ //If Request Generated From Ajax
                //Getting Posted Values
                $ID = $this->input->post('ID');
                $Table = $this->input->post('Table');
                $Column = $this->input->post('Column');
                $PrimaryKey=$this->input->post('PrimaryKey');
                $updateData = array(
                    $Column =>'0'
                );
                $updateWhere = array(
                    $PrimaryKey => $ID
                );
                $result = $this->Common_model->update($Table,$updateWhere,$updateData);
                if($result === true){
                    echo "OK::Record Successfully Deleted::success";
                    return;
                }else{
                    if($result['code'] === 0){
                        echo "FAIL::RecordNotDeleted::error";
                    }else{
                        echo "FAIL::";print_r($result); echo "::error";
                    }
                    return;
                }
            }
        }
    }

    //Temporary Save Image for Misc Work
    public function saveImageTemp(){
        $post = isset($_POST) ? $_POST: array();
        $max_width = "500";
        $userId = isset($post['hdn-profile-id']) ? intval($post['hdn-profile-id']) : 0;
        $path = './uploads/profile_pictures/'.$this->data['UserID'].'-'.$this->data['Username'].'/originals';


        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");

        $name = $_FILES['photoimg']['name'];
        $size = $_FILES['photoimg']['size'];
        if(strlen($name))
        {
            list($txt, $ext) = explode(".", $name);
            if(in_array(strtolower($ext),$valid_formats))
            {
                if($size<(1024*1024)) // Image size max 1 MB
                {
                    $actual_image_name = 'orig' .'_'.time() .'.'.$ext;
                    $filePath = $path .'/'.$actual_image_name;
                    $tmp = $_FILES['photoimg']['tmp_name'];

                    //Remove File if Exist with same name in the directory.
                    //Hopefully it will not be removed as names will almost never gonna be same
                    if (file_exists($filePath)) {
                        unlink($filePath);
                    }

                    if(move_uploaded_file($tmp, $filePath))
                    {
                        $width = getWidth($filePath);
                        $height = getHeight($filePath);
                        //Scale the image if it is greater than the width set above
                        if ($width > $max_width){
                            $scale = $max_width/$width;
                            $uploaded = resizeImage($filePath,$width,$height,$scale);
                        }else{
                            $scale = 1;
                            $uploaded = resizeImage($filePath,$width,$height,$scale);
                        }

                        //Now Lets Update Database for Original Image.
                        $table = "um_users";
                        $where = array(
                            'UserID' => $this->data['UserID']
                        );
                        $data = array(
                            'profileImageOriginal' => $actual_image_name,
                            'DateUpdated' => date($this->data['MySQLDateTimeFormat'])
                        );
                        //Finally Just Call the Model Method to Update the DB.
                        $this->Common_model->update($table,$where,$data);

                        echo "<img id='photo' file-name='".$actual_image_name."' class='' src='".base_url().'uploads/profile_pictures/'.$this->data['UserID'].'-'.$this->data['Username'].'/originals/'.$actual_image_name."' class='preview'/>";
                    }
                    else
                        echo "failed";
                }
                else
                    echo "FAIL::Image file size max 1 MB::error";
            }
            else
                echo "Invalid file format..";
        }
        else
            echo "Please select image..!";
        exit;
    }

    function cropAvatarImage()
    {
        $post = isset($_POST) ? $_POST: array();
        $userId = isset($post['id']) ? intval($post['id']) : 0;
        $thumbsPath = './uploads/profile_pictures/'.$this->data['UserID'].'-'.$this->data['Username'].'/thumbs/';
        $originalsPath = './uploads/profile_pictures/'.$this->data['UserID'].'-'.$this->data['Username'].'/originals/';
        $t_width = 128; // Maximum thumbnail width
        $t_height = 128;    // Maximum thumbnail height

        $imageName = $this->input->post("image_name");
        $imagePath = $originalsPath.$imageName;
        $thumbsImageName = explode('orig_',$imageName);
        $thumbsImageName = end($thumbsImageName);
        $thumbsImageName = 'thumb_' .$thumbsImageName;

        //If Directory Not Available then create one.
        if(!file_exists($thumbsPath)){
            mkdir($thumbsPath,0777,TRUE);
        }

        if(isset($_POST['t']) and $_POST['t'] == "ajax")
        {
            extract($_POST);
            //If Not Cropped Then Just Resize the Image.
            if(empty($x1) || empty($w1)){
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $originalsPath.$imageName;
                $config['thumb_marker'] = FALSE;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['height'] = 128;
                $config['width'] = 128;
                $config['new_image'] = $thumbsPath.$thumbsImageName;//you should have write permission here..
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
            }else{

                //Getting All Posted Values
                $w1 = $this->input->post('w1');
                $h1 = $this->input->post('h1');
                $x1 = $this->input->post('x1');
                $y1 = $this->input->post('y1');

                $thumbsImagePath = $thumbsPath.$thumbsImageName;
                $ratio = ($t_width/$w1);
                $nw = ceil($w1 * $ratio);
                $nh = ceil($h1 * $ratio);
                $nimg = imagecreatetruecolor($nw,$nh);
                $im_src = imagecreatefromjpeg($imagePath);
                imagecopyresampled($nimg,$im_src,0,0,$x1,$y1,$nw,$nh,$w1,$h1);
                imagejpeg($nimg,$thumbsImagePath,90);
            }

            //Now Finally Update the Database for the Cropped Image.
            $table = "um_users";
            $where = array(
                "UserID" => $this->data['UserID']
            );

            $data = array(
                'profileImageCropped' => $thumbsImageName,
                'DateUpdated' => date($this->data['MySQLDateTimeFormat'])
            );

            //Finally Run the Method of Model to Update the Table
            $this->Common_model->update($table,$where,$data);
        }

        echo base_url().'uploads/profile_pictures/'.$this->data['UserID'].'-'.$this->data['Username'].'/thumbs/'.$thumbsImageName;
        exit(0);
    }
}