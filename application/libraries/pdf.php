<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 11/7/2015
 * Time: 11:33 AM
 */


class pdf {

    function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }

    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

            switch($param){
                case "L":
                    $margin_left = 10;
                    $margin_right = 10;
                    $margin_top = 50;
                    $margin_bottom = 10;
                    $margin_header = 10;
                    $margin_footer = 5;

                    $param = array('utf-8', 'A4-L','','',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
                    break;
                case "P":
                    $param = '"en-GB-x","A4","","",10,10,10,10,6,3';
                    break;
                default:
                    if(!isset($param) || empty($param) || $param === NULL){
                        $param = '"en-GB-x","A4","","",10,10,10,10,6,3';
                    }
                break;
            }
/*            $param = '"en-GB-x","A4","","",10,10,10,10,6,3'; // Defaults to portrait
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3,"P"'; // Portrait
            $param = '"en-GB-x","A4-L","","",10,10,10,10,6,3,"L"'; // Landscape*/

        if(is_array($param)){
            return new mPDF($param[0],$param[1],$param[2],$param[3],$param[4],$param[5],$param[6],$param[7],$param[8],$param[9]);
        }else{
            return new mPDF($param);
        }
    }
}