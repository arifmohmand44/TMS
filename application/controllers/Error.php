<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 12/25/2015
 * Time: 10:36 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    //Page 404
    public function P_404(){
        $this->show('');
    }


}