<?php
/**
 * Created by Arif Mohmand.
 * User: COD3R
 * Date: 1/14/2017
 * Time: 10:55 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('Role') !== 'Admin') {
            switch ($this->session->userdata('Role')) {
                case 'Teacher':
                    redirect('Teacher/index');
                    break;
            }
        };
    }

    public function index()
    {
        $this->data['PageTitle'] = "Dashboard";
        $this->show('admin/dashboard', $this->data);
    }
    //User Profile
    public function profile()
    {

        //Get User Profile Data
        $table = 'tms_users U';
        $where = array(
            'U.UserID' => $this->data['UserID']
        );
        $joins = array(
            array(
                'table' => 'tms_administrator A',
                'condition' => 'A.UserID = U.UserID',
                'type' => 'LEFT'
            )
        );
        $selectData = array(
            'U.UserID, FullName AS Name, ContactNo AS Contact, Address AS Address,U.Username, U.Email, U.profileImageCropped AS Avatar',
            false
        );

        $this->data['AdminProfileDetails'] = $this->Common_model->select_fields_where_like_join($table, $selectData, $joins, $where, true);
        $this->data['PageTitle'] = "User Profile";
        $this->show('admin/profile', $this->data);
    }
    //Update User Profile
    public function UpdateProfile()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                //If Came Through a proper Channel Then User Should Be Able To Update his/her Profile.

                //Lets Do Form Validation First.
                $Name = $this->input->post('Name');
                $Address = $this->input->post('Address');
                $Contact = $this->input->post('Contact');
                $Email = $this->input->post('Email');

                //Password-Security Fields
                $CurrentPass = $this->input->post('CurrentPass');

                $NewPass = $this->input->post('NewPass');
                $ConfirmNewPass = $this->input->post('ConfirmNewPass');

                //First Lets Check If Pass is Ok.
                $UserID = $this->data['UserID'];
                $UserRole = $this->data['Role'];

                $userTable = 'tms_users';
                $selectData = 'Password, GroupID';
                $whereUser = array(
                    'UserID' => $UserID
                );
                $userCurrentPass = $this->Common_model->select_fields_where($userTable, $selectData, $whereUser, true);

                if (isset($userCurrentPass) && !empty($userCurrentPass)) {
                    $userCurrentPassword = $userCurrentPass->Password;
                    $userGroupID = $userCurrentPass->GroupID;
                } else {
                    echo "FAIL::Could Not Find User, Some Error Occurred::error";
                    return;
                }

                if (empty($CurrentPass) || !isset($CurrentPass)) {
                    echo "FAIL:: Please Provide Current Password, To update your profile data.::error";
                    return;
                }

                if ($CurrentPass === $userCurrentPassword) {
                    if ($NewPass !== $ConfirmNewPass) {
                        echo "FAIL::Passwords Do Not Match::error";
                        return;
                    }

                    //For User Table
                    $updateUserData = array(
                        'Email' => $Email,
                        'UpdatedBy' => $UserID,
                        'DateUpdated' => $this->data['dbCurrentDateTime']
                    );
                    if (!empty($NewPass) || !empty($NewPass)) {
                        $updateUserData['Password'] = $NewPass;
                    }

                    //For Role Table
                    $rTable = 'tms_administrator';
                    $updateRoleData = array(
                        'FullName' => $Name,
                        'Address' => $Address,
                        'ContactNo' => $Contact
                    );
                    $whereRTable = array(
                        'UserID' => $UserID
                    );


                    //Now As Its Split In to Two Tables So We Will Have to Update Two Tables Separately

                    //First Update User Data
                    $result = $this->Common_model->update($userTable, $whereUser, $updateUserData);
                    if ($result !== true) {
                        if ($result['code'] !== 0) {
                            echo "FAIL::" . $result['message'] . "::error";
                            return;
                        }
                    }

                    $result2 = $this->Common_model->update($rTable, $whereRTable, $updateRoleData);

                    if ($result2 !== true) {
                        if ($result2['code'] !== 0) {
                            echo "FAIL::" . $result2['message'] . "::error";
                            return;
                        }
                    }

                    if ($result !== true && $result2 !== true) {
                        echo "FAIL::Something Went Wrong with the Update, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }

                    echo "OK::Record Successfully Updated::success";
                    return;

                } else {
                    echo "FAIL::Wrong Password::error";
                    return;
                }

            }
        }
    }
    //System Configuration
    public function system($param = NULL){
        if($this->input->post()){
            if($param === "update"){
                //Posted Values
                $fieldName = $this->input->post("name");
                $fieldValue = $this->input->post("value");

                $updateTable = "sys_settings";
                $where = array(
                    'Type' => $fieldName
                );
                $updateData = array(
                    'Value' => $fieldValue
                );

                $result = $this->Common_model->update($updateTable,$where,$updateData);
                if($result === true){
                    echo "OK::Record Successfully Updated::success";
                }else{
                    if($result['code'] === 0){
                        echo "FAIL::Record with same value already exist::warning";
                    }else{
                        echo "FAIL::";print_r($result); echo "::error";
                    }
                }
                return;
            }
        }

        //Getting System Configuration Information For View.
        $table = 'sys_settings SC';
        $selectData = array(
            'CASE SC.Type
             WHEN "SiteName" THEN SC.Value
             ELSE "" END AS SiteName,

             CASE SC.Type
             WHEN "CompanyName" THEN SC.Value
             ELSE "" END AS CompanyName,

             CASE SC.Type
             WHEN "CompanyAddress" THEN SC.Value
             ELSE "" END AS CompanyAddress,

             CASE SC.Type
             WHEN "CompanyPhone" THEN SC.Value
             ELSE "" END AS CompanyPhone,

             CASE SC.Type
             WHEN "CompanyEmail" THEN SC.Value
             ELSE "" END AS CompanyEmail',
            false
        );


        $systemInfo = $this->Common_model->select_fields_where_like_join($table,$selectData);
        $systemInfo = json_decode(json_encode($systemInfo),true);

        $this->data['systemConfiguration'] = array();
        if(isset($systemInfo) && is_array($systemInfo) && !empty($systemInfo)){
            foreach($systemInfo as $key=>$innerArray){
                foreach($innerArray as $key=>$val){
                    if(!empty($val) && $key === "SiteName"){
                        $this->data['systemConfiguration']['SiteName'] = $val;
                    }
                    if(!empty($val) && $key === "CompanyName"){
                        $this->data['systemConfiguration']['CompanyName'] = $val;
                    }
                    if(!empty($val) && $key === "CompanyAddress"){
                        $this->data['systemConfiguration']['CompanyAddress'] = $val;
                    }
                    if(!empty($val) && $key === "CompanyPhone"){
                        $this->data['systemConfiguration']['CompanyPhone'] = $val;
                    }
                    if(!empty($val) && $key === "CompanyEmail"){
                        $this->data['systemConfiguration']['CompanyEmail'] = $val;
                    }
                }
            }
        }
        $this->data['PageTitle'] = 'System Configuration';
        $this->show('admin/system/system',$this->data);
    }
    //This Method will Have a View For Users List
    public function users($param = NULL)
    {
        if ($this->input->post()) {
            //User Data table List
            if ($param === 'list') {
                //Means Request Generated By DataTables, So Response back the Json Result.

                $table = 'tms_users U';
                $selectData = array('
                U.UserID AS ID,
                U.Username AS UName,
                U.Email AS Email,
                U.isActive AS Active,
                UC.Username AS CBy,
                U.DateCreated AS DateCreated,
                UP.Username AS UBy,
                CASE WHEN U.IsActive = "1" THEN (CONCAT("<span class=\"label label-success\">","<span class=\"fa fa-unlock\"></span> Active","</span>")) ELSE (CONCAT("<span class=\"label label-danger\">","<span class=\"fa fa-lock\"></span> De-Active","</span>")) END AS Status,
                U.IsActive AS StatusID,
                Name AS GName,', false);
                $joins = array(
                    array(
                        'table' => 'tms_users UC',
                        'condition' => 'UC.UserID = U.CreatedBy',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'tms_users UP',
                        'condition' => 'UP.UserID = U.UpdatedBy',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'tms_users_groups G',
                        'condition' => 'U.GroupID = G.GroupID',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'tms_administrator A',
                        'condition' => 'A.UserID = U.UserID',
                        'type' => 'LEFT'
                    )
                );

                $where = "1=1";

                $groupSelector = $this->input->post('groupSelector');
                $ActiveSelect = $this->input->post('ActiveSelect');
                //$where = 'U.IsActive = 1';

                //Lets Check If There were any filters Applied to this.

                if (isset($groupSelector) && is_numeric($groupSelector) && !empty($groupSelector)) {
                    $where .= ' AND U.GroupID = ' . $groupSelector;
                }

                if (isset($ActiveSelect) && is_numeric($ActiveSelect)) {
                    if ($ActiveSelect != "2") { //2. is for the All
                        $where .= ' AND U.IsActive = ' . $ActiveSelect;
                    }
                }

                //Action Column Need To Be Added
                $add_column = array(
                    'ViewEditActionButtons' => array('<a href="' . base_url() . 'Admin/users/edit/$1" class="disciplinaryActionReview"><span class="fa fa-pencil-square-o"></span></a>&nbsp; &nbsp;&nbsp;  |  &nbsp;&nbsp;&nbsp; <a style="cursor:pointer;cursor:hand;" data-toggle="modal" data-target=".deleteUser-modal" class="disciplinaryActionReview"><span class="fa fa-lock text-red"></span></a>', 'ID')
                );
                $order_by =array('U.UserID','DESC');
                $users = $this->Common_model->select_fields_joined_DataTable($selectData,$table,$joins,$where,$order_by,'','','',$add_column);
                $usersArray = json_decode($users,true);

                foreach($usersArray['aaData'] as $key => $user){
                    if($user['StatusID'] === '0'){
                        $temp = $usersArray['aaData'][$key]['ViewEditActionButtons'];
                        $exploded = explode('|',$temp);
                        $usersArray['aaData'][$key]['ViewEditActionButtons'] =
                            $exploded[0] . ' | &nbsp;&nbsp;&nbsp; ' . '<a style="cursor:pointer;cursor:hand;" data-toggle="modal" data-target=".enableUser-modal" class="enableUser"><span class="fa fa-unlock text-green"></span></a>';
                    }
                }

                $users = json_encode($usersArray);

                print $users;
                return;
            }
        }
        //Add New User
        if ($param === "add") {
            if ($this->input->post()) {
                /*                var_dump($this->input->post());
                exit;*/
                //Patient Details
                $userName = $this->input->post('userName');
                $fullName = $this->input->post('fullName');
                $password = $this->input->post('userPassword');
                $confirmPassword = $this->input->post('confirmPassword');
                $Email = $this->input->post('userEmail');
                $groupSelector = $this->input->post('groupSelector');
                $loggedInUserID = $this->data['UserID'];
                $CurrentDateTime = $this->data['dbCurrentDateTime'];

                //Little Validations.

                //Email Must Be In Proper Format.
                if (!filter_var($Email, FILTER_VALIDATE_EMAIL)) {
                    $alertMsg = $this->lang->line('EmailisNotInValidFormat');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    redirect('add-users');
                }



                $UserTable = "tms_users";

                //Before Create New User, We First Need To Query Database to Know if This Email Has Already Been Used Or Not.
                $selectData = array("COUNT(1) AS TotalRecordsFound", false);
                $where = "Email = '" . $Email . "'";
                $result = $this->Common_model->select_fields_where($UserTable, $selectData, $where, TRUE);

                if ($result->TotalRecordsFound > 0) {
                    $alertMsg = "Username or Email Already Exist::error::DB Error";
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    redirect('add-users');
                    return;
                }

                $UserData = array(
                    'Username' => $fullName,
                    'Email' => $Email,
                    'Password' => $password,
                    'GroupID' => $groupSelector,
                    'CreatedBy' => $loggedInUserID,
                );


                $this->db->trans_begin();
                $UserID = $this->Common_model->insert_record($UserTable, $UserData);

                if (!isset($UserID) || empty($UserID) || !is_numeric($UserID)) {
                    $alertMsg = "Something Is wrong with the User Information, Can't Add User::error::DB Error";
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    $this->db->trans_rollback();
                    redirect('add-users');
                }

                //If Successfully Added User Than Add the User Information in the Required Table.

                switch ($groupSelector) {
                    case 1:
                        $table = "tms_administrator";
                        $tableInsertData = array(
                            'FullName' => $fullName,
                            'DateCreated' => $this->data['dbCurrentDateTime'],
                            'UserID' => $UserID,
                            'CreatedBy' => $loggedInUserID,
                        );
                        break;
                    case 2:
                        $table = "tms_teacher";
                        $tableInsertData = array(
                            'FullName' => $fullName,
                            'DateCreated' => $this->data['dbCurrentDateTime'],
                            'UserID' => $UserID,
                            'CreatedBy' => $loggedInUserID,
                        );
                        break;
                    case 3:
                        $table = "tms_student";
                        $tableInsertData = array(
                            'FullName' => $fullName,
                            'DateCreated' => $this->data['dbCurrentDateTime'],
                            'UserID' => $UserID,
                            'CreatedBy' => $loggedInUserID,
                        );
                        break;
                    default:
                        $alertMsg = "Can\'t Add User::error::DB Error";
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                        $this->db->trans_rollback();
                        redirect('add-users');
                        break;
                }

                $this->Common_model->insert_record($table, $tableInsertData);

                if ($this->db->trans_status() === FALSE) {
                    $alertMsg = "Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error::DB Error";
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    $this->db->trans_rollback();
                    redirect('add-users');
                } else {
                    $alertMsg = "Record Successfully Added To The System::success";
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    $this->db->trans_commit();
                    redirect('users');
                }
            }
            $this->data['PageTitle'] = 'Add User';
            $this->show('admin/users/add_user', $this->data);
            return;
        }
        //Update User
        if ($param === "edit") {
            $UserID = $this->uri->segment(4);
            if (!is_numeric($UserID)) {
                $alertMsg = "Error Processing Your Required, User ID must be numeric::error";
                $this->session->set_flashdata('alertMsg',$alertMsg);
                redirect(previousURL());
                return;
            }

            //Getting User Details.
            $UsersTable = "um_users U";
            $selectData = '*';
            $where = array(
                'U.UserID' => $UserID
            );

            $this->data['UserDetails'] = $this->Common_model->select_fields_where($UsersTable, $selectData, $where, TRUE);



            //Getting Speciality Details.
            $DoctorTable = "hms_doctor D";
            $selectData = 'D.Speciality AS ID, MLS.Speciality';
            $joins = array(
                array(
                    'table' => 'ml_medical_speciality MLS',
                    'condition' => 'D.Speciality = MLS.ID',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'D.UserID' => $UserID,
            );

            $this->data['SpecialityDetails'] = $this->Common_model->select_fields_where_like_join($DoctorTable,$selectData, $joins, $where, TRUE);

            //If No Record Found For the Posted UserID
            if (empty($this->data['UserDetails'])) {
                $alertMsg = "No Record Found For The Selected User::error::Db Error";
                $this->session->set_flashdata('alertMsg', $alertMsg);
                redirect(previousURL());
            }


            //First Lets Check if its the Post, If Not then we need to show the view with Current Values.
            if ($this->input->post()) {

                //Getting Posted Values First
                $fullName= $this->input->post("userName");
                $userPassword = $this->input->post("userPassword");
                $userEmail = $this->input->post("userEmail");
                $loggedInUserID = $this->data['UserID'];
                //User Posted Group ID
                $userPostedGroupID = $this->input->post("groupSelector");

                if($userPostedGroupID == '5'){
                    $specialityID = $this->input->post("specialitySelector");

                    //Redirect User If he selected the GroupID of Consultant
                    // but has not Provided the SpecialityID

                    if(empty($specialityID)){
                        $alertMsg = "Please selected speciality from the DropDown::error";
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                        redirect('Admin/users/edit/' . $UserID);
                    }
                    //Need To Work For Update Speciality.
                    $DocTable ="hms_doctor D";
                    $updateSpeciality = array(
                        'Speciality' =>$specialityID
                    );
                    $where = array(
                        'D.UserID' => $UserID
                    );
                    $this->Common_model->update($DocTable, $where, $updateSpeciality);

                }


//                if ($userPassword !== $this->data['UserDetails']->Password) {
//                    $alertMsg = $this->lang->line('msgIncorrectPassword');
//                    $this->session->set_flashdata('alertMsg', $alertMsg);
//                    redirect('Admin/users/edit/' . $UserID);
//                }

                //If Email is Entered
                if (!isset($userEmail) || empty($userEmail)) {
                    $alertMsg = $this->lang->line('msgEmailisRequired');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    redirect('add-users');
                }
                //Email Must Be In Proper Format.
                if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
                    $alertMsg = $this->lang->line('msgEmailisNotInValidFormat');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    redirect('add-users');
                }

                //Need To Work For Update Here.
                $UsersTable = "um_users U";
                $updateData = array(
                    'GroupID' =>$userPostedGroupID,
                    'Username' =>$fullName,
                    'Password' =>$userPassword,
                    'Email' =>$userEmail,
                    'UpdatedBy' =>$loggedInUserID
                );
                $where = array(
                    'U.UserID' => $UserID
                );
                $this->Common_model->update($UsersTable, $where, $updateData);


                if ($this->db->trans_status() === FALSE) {
                    $alertMsg = "Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error::DB Error";
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    $this->db->trans_rollback();
                    redirect('Admin/users/edit');
                } else {
                    $alertMsg = $this->lang->line('msgRecordSuccessfullyUpdatedToTheSystem');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    $this->db->trans_commit();
                    redirect('Admin/users');
                }

                redirect('admin/users');
                return false;
            } //End of $this->input->post() if statement

            //User Existing GroupID
            $userGroupID = $this->data['UserDetails']->GroupID;

            $UserGroupDbTitle = $this->Common_model->select_fields_where('um_users_groups', 'Name', array('GroupID' => $userGroupID), TRUE);

            $this->data['UserDetails']->GroupTitle = $UserGroupDbTitle->Name;

            //Showing the View.
            $this->data['PageTitle'] = 'Edit User';
            $this->show('admin/users/edit_user', $this->data);
            return;
        }
        //For User Active
        if ($param === "enable"){
            if($this->input->post()){ //If Any Values Posted
                if($this->input->is_ajax_request()){ //If Request Generated From Ajax
                    //Getting Posted Values
                    $ID = $this->input->post('ID');
                    $Table = $this->input->post('Table');
                    $Column = $this->input->post('Column');
                    $PrimaryKey=$this->input->post('PrimaryKey');
                    $updateData = array(
                        $Column =>'1'
                    );
                    $updateWhere = array(
                        $PrimaryKey => $ID
                    );
                    $result = $this->Common_model->update($Table,$updateWhere,$updateData);
                    if($result === true){
                        echo "OK::User Login Successfully Activated::success";
                        return;
                    }else{
                        if($result['code'] === 0){
                            echo "FAIL::Could Not Activate The User, User Might Already is Activated::warning";
                        }else{
                            echo "FAIL::";print_r($result); echo "::error";
                        }
                        return;
                    }
                }
            }
        }
        //For User De-Active
        if ($param === "disabled"){
            if($this->input->post()){ //If Any Values Posted
                if($this->input->is_ajax_request()){ //If Request Generated From Ajax
                    //Getting Posted Values
                    $ID = $this->input->post('ID');
                    $Table = $this->input->post('Table');
                    $Column = $this->input->post('Column');
                    $PrimaryKey=$this->input->post('PrimaryKey');
                    $updateData = array(
                        $Column =>'0'
                    );
                    $updateWhere = array(
                        $PrimaryKey => $ID
                    );
                    $result = $this->Common_model->update($Table,$updateWhere,$updateData);
                    if($result === true){
                        echo "OK::User Login Successfully De-Activated::success";
                        return;
                    }else{
                        if($result['code'] === 0){
                            echo "FAIL::Could Not De-Activated The User, User Might Already is De-Activated::warning";
                        }else{
                            echo "FAIL::";print_r($result); echo "::error";
                        }
                        return;
                    }
                }
            }
        }

        $this->data['PageTitle'] = 'Master User';
        $this->show('admin/users/list',$this->data);
    }
}