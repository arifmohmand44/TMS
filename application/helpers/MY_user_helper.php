<?php
/**
 * Created by PhpStorm.
 * User: COD3R
 * Date: 9/18/2015
 * Time: 12:04 PM
 */

/**
 * @var Common_Model $Common_Model It resides all the methods which can be used in most of the controllers.
 * @property Common_Model $Common_Model It resides all the methods which can be used in most of the controllers.
 */
if(!function_exists('getUserProfile')) {
    function getUserProfile($userID,$Role)
    {
        if(!is_numeric($userID) || empty($userID)){
            return;
        }

        $ci =& get_instance();
        $ci->load->model('Common_Model');

        //Query To Load Menus For the Current User.
        switch($Role){
            case 'Admin':
                //Query To Get Profile Data From Admin Table
                $table = 'tms_administrator';
                $selectData = '*';
                $where = array('UserID'=>$userID);
                break;
            default:
                return false;
        }

        //Finally Executing the Query
        $userProfile = $ci->Common_Model->select_fields_where($table,$selectData,$where,TRUE);
        return $userProfile;
    }
}