<h1>ABSTRCT</h1>
<pre>
This Application will be developed to support the management and the activities of RSH in Pakistan.
This System will be based on online Web Application where different members of different groups will have access to system up to the certain point of need, So That each and every group must perform its own duties faithfully under their own department, and un-authorized members or groups will not be able to access to restricted modules.

This Application will consist of the following modules or more (Depending on needs.):
    1.	Patient Admission
    2.	OT Bookings
    3.	OT Pharmacy
    4.	Laboratory
    5.	Accounts
    6.	User Management/Permissions
    7.	System Configurations (Includes Master lists for different departments etc.)
    8.	Reports (Different Reports like Revenue Report etc.)
This document will describe how all the modules mentioned above will work together and will give insight about this application for its proper and correct use.
This System will have pre-defined Groups, which includes:
    1.	Administrator
    2.	Admission/Receptionist (Person Responsible for Admitting Patient, So that the flow continues )
    3.	Doctor
    4.	Laboratory technician
    5.	Accountant
    6.	Pharmacist
    
    
    =-=-=-=-==-=-=-=--
    Update: Project Forked To Adding/Updating the new requirements on it.
    
    New Requirements Currently Includes:
    1. Adding of Portal Support for Doctor/Patients/Nurses where they can do their respective work/tasks.
    2. Appointments/ Health Records System.

</pre>