<?php

//error_reporting(0); //Setting this to E_ALL showed that that cause of not redirecting were few blank lines added in some php files.

$db_config_path = '../application/config/production/database.php';

// Only load the classes in case the user submitted the form
if($_POST) {

    // Load the classes and create the new objects
    require_once('includes/core_class.php');
    require_once('includes/database_class.php');

    $core = new Core();
    $database = new Database();


    //Step 1 : Database Setup
    if($_POST['step'] == "dbSetup"){

        // Validate the post data for Step 1
        if($core->validate_post($_POST) == true)
        {
            // First create the database, then create tables, then write config file
            if($database->create_database($_POST) == false) {
                echo "FAIL::The database could not be created, please verify your settings.::error";
            } else if ($database->create_tables($_POST) == false) {
                echo "FAIL::The database tables could not be created, please verify your settings.::error";
            } else if ($core->write_config($_POST) == false) {
                echo "FAIL::The database configuration file could not be written, please chmod application/config/database.php file to 777::error";
            }

            // If no errors, redirect to Step 2
            echo "OK::Successfully Created the Database::success";
        }
        else {
            echo "FAIL::Not all fields have been filled in correctly. The host, username, password, and database name are required.::error";
        }
    }

    //Step 2 : Hospital Setup
    if($_POST['step'] == "hospitalSetup"){
        $hospitalFULLName = $_POST['HospitalName'];
        $SiteName = $_POST['SiteName'];
        $HospitalAddress = $_POST['HospitalAddress'];

        if(empty($hospitalFULLName)){
            echo "FAIL::Hospital Name is required::error";
            exit;
        } else if(empty($hospitalFULLName)){
            echo "FAIL::Site Name is required::error";
            exit;
        }

        if($database->insert_hospital_details($_POST) === false){
            echo "FAIL::Something went Wrong with the Details. Could Not Create Company Details::error";
            exit;
        };

        if ($core->write_config($_POST) == false) {
            echo "FAIL::The database configuration file could not be written, please chmod application/config/database.php file to 777::error";
            exit;
        }

        if($core->update_baseURL($_POST) === false){
            echo "FAIL::Could Not Update the Cofig File::error";
            exit;
        }

    }

    //Step 3 : Admin User Setup
    if($_POST['step'] == "userSetup"){

        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = trim($_POST['password']);
        $confirmPass = trim($_POST['confirmPass']);


        if(empty($username)){
            echo "FAIL::Username can not be less than 3 characters::error";
            return false;
        } else if(empty($email)){
            echo "FAIL::Email Is a Required Field::error";
            return false;
        } else if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            echo "Invalid Email Address";
            return false;
        } else if(empty($password)){
            echo "FAIL::No Password has been specified for user::error";
            return false;
        }else if(strlen($password) < 4){
            echo "FAIL::Password Can Not Be Less than 4 Characters::error";
            return false;
        }else if($password !== $confirmPass){
            echo "FAIL::Passwords Do Not Match::error";
            return false;
        }

        if($database->insert_user_details($_POST) === true){
            echo "OK::Application Successfully Installed::success";
            return null;
        }

    }

}

?>