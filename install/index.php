<?php
		error_reporting(0); //Setting this to E_ALL showed that that cause of not redirecting were few blank lines added in some php files.
		$db_config_path = '../application/config/production/database.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminLTE 2 | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="../assets/plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
		.nav-pills{
			border-bottom: 1px solid whitesmoke;
		}
		.footer{
			clear: both;
			background: white;
			padding: 1%;
			margin-top: 1%;
		}
		.error {
			background: #ffd1d1;
			border: 1px solid #ff5858;
			padding: 4px;
		}
		#myWizard{
			margin-top:6%;
		}
		.box .overlay, .overlay-wrapper .overlay{
			z-index:1050;
		}
	</style>
</head>
<body class=" hold-transition login-page">
<header class="main-header" style="background: #3C8DBC; color:white; ">
	<a href="" class="logo" style="color: white; width: 30%;">
		<!-- LOGO -->
		Hospital Management System
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">

		</div>
	</nav>
</header>
<section class="row">
<div class="col-md-2 col-xs-2 col-sm-2"></div>
<div class="row col-md-6 col-xs-10 col-xs-offset-1" id="myWizard">
		<div class="login-logo">
			<a href=""><b style="color: green;	">Installation </b> Steps</a>
		</div><!-- /.login-logo -->
		<hr>
		<div class="progress">
			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="5" style="width: 20%;">
				Step 1 of 3
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="step1">
					<div class="box box-info well">
						<div class="box-header with-border">
							<h3 class="box-title">Database Setup</h3>
						</div>
						<form id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						<div class="box-body">
							<?php if(is_writable($db_config_path)){?>

								<?php if(isset($message)) {

									echo '<p class="error">' . $message . '</p>';
								}
								?>
								<p class="error" style="display: none;"></p>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="hostname">Hostname</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-random"></i></span>
												<input type="text" class="form-control" id="hostname" value="localhost" name="hostname">
											</div><!-- /input-group -->
										</div><!-- /col -->

										<div class="form-group col-md-6">
											<label for="dbName">Database Name</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-database"></i></span>
												<input type="text" class="form-control" id="dbName" name="database" placeholder="Database">
											</div><!-- /input-group -->
										</div><!-- /col -->
									</div><!-- /row -->
									<div class="row">
										<div class="form-group col-md-6">
											<label for="dbUsername">Username</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
												<input type="text" class="form-control" id="dbUsername" name="username" placeholder="User Name">
											</div><!-- /input-group -->
										</div><!-- /col -->
										<div class="form-group col-md-6">
											<label for="dbPassword">Password</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-lock"></i></span>
												<input type="password" class="form-control" id="dbPassword" name="password" placeholder="Password">
											</div><!-- /input-group -->
										</div><!-- /col -->
									</div><!-- /row -->
							<?php } else { ?>
								<p class="error">Please make the application/config/production/database.php file writable. <strong>Example</strong>:<br /><br /><code>chmod 777 application/config/database.php</code></p>
							<?php } ?>

						</div><!-- /.box-body -->
						<br>
						<div class="box-footer">
							<a class="btn btn-info next pull-right" id="nextBtn" href="#">Next</a>
						</div><!-- /.box-footer -->
						</form>
					</div><!-- /.box -->
			</div>
			<div class="tab-pane fade" id="step2">
				<div class="box box-info well">
					<div class="box-header with-border">
						<h3 class="box-title">Hospital Setup</h3>
					</div>
					<div class="box-body">
						<p class="error" style="display: none;"></p>
							<form id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
								<div class="row">
									<div class="form-group col-md-6">
										<label for="hospitalName">Hospital Name</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-bank"></i></span>
											<input type="text" class="form-control" id="hospitalName" name="Hname" placeholder="Hospital Name">
										</div><!-- /input-group -->
									</div><!-- /col -->
									<div class="form-group col-md-6">
										<label for="HospitalAddress">Address</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-home"></i></span>
											<input type="text" class="form-control" id="HospitalAddress" name="HAddress" placeholder="Address">
										</div><!-- /input-group -->
									</div><!-- /col -->
								</div><!-- /row -->
								<div class="row">
									<div class="form-group col-md-6">
										<label for="siteName">Site Name</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-globe"></i></span>
											<input type="text" class="form-control" id="siteName" name="siteName" placeholder="Site Name">
										</div><!-- /input-group -->
									</div><!-- /col -->
									<div class="form-group col-md-6">
										<label for="siteURL">Site URL</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-link"></i></span>
											<input type="text" class="form-control" id="siteURL" name="siteURL" placeholder="Base URL">
										</div><!-- /input-group -->
									</div><!-- /col -->
								</div><!-- /row -->
							</form>
					</div><!-- /.box-body -->
					<br>
					<div class="box-footer">
						<a class="btn btn-default previous" href="#">Back</a>
						<a class="btn btn-info next pull-right" id="nextBtn" href="#">Next</a>
					</div><!-- /.box-footer -->
				</div><!-- /.box -->
			</div>
			<div class="tab-pane fade" id="step3">
				<div class="box box-info well">
					<div class="box-header with-border">
						<h3 class="box-title">Admin / Users Setup</h3>
					</div>
					<div class="box-body">
						<p class="error" style="display: none;"></p>
							<form id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
								<div class="row">
									<div class="form-group col-md-6">
										<label for="username">User Name</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-user"></i></span>
											<input type="text" class="form-control" id="username"  name="username" placeholder="User Name">
										</div><!-- /input-group -->
									</div><!-- /col -->
									<div class="form-group col-md-6">
										<label for="email">Email</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-at"></i></span>
											<input type="text" class="form-control" id="email" name="email" placeholder="Email">
										</div><!-- /input-group -->
									</div><!-- /col -->
								</div><!-- /row -->
								<div class="row">
									<div class="form-group col-md-6">
										<label for="password">Password</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-lock"></i></span>
											<input type="text" class="form-control" id="password" name="password" placeholder="Password">
										</div><!-- /input-group -->
									</div><!-- /col -->
									<div class="form-group col-md-6">
										<label for="confirmPass">Confirm Password</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-lock"></i></span>
											<input type="password" class="form-control" id="confirmPass" name="confirmPass" placeholder="Confirm Password">
										</div><!-- /input-group -->
									</div><!-- /col -->
								</div><!-- /row -->
							</form>
					</div><!-- /.box-body -->
					<br>
					<div class="box-footer">
						<input type="button" class="btn btn-default back previous" value="Back">
						<a class="btn btn-success first pull-right" id="finishBtn" href="#">Finish</a>
					</div><!-- /.box-footer -->
				</div><!-- /.box -->
			</div>
		</div>
	    <div class="navbar">
		  <div class="navbar-inner">
			<ul class="nav nav-pills">
				<li class="active"><a href="#step1" data-toggle="tab" data-step="1">Step1: Database Setup</a></li>
				<li><a href="#step2" data-toggle="tab" data-step="2">Step2: Hospital Setup</a></li>
				<li><a href="#step3" data-toggle="tab" data-step="3">Step3: Admin/User Setup</a></li>
			</ul>
		  </div>
	    </div>
	</div><!-- /.login-box-body -->
</section><!-- /.login-box -->
<footer class="footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 1.0 (beta)
	</div>
	<strong>Copyright © 2015 <a href="http://parexons.com/">Parexons</a>.</strong> All rights reserved.
</footer>
<!-- jQuery 2.1.4 -->
<script src="../assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../assets/plugins/iCheck/icheck.min.js"></script>
<script>

	$.fn.extend({
		myAjaxFunc: function () {

			this.each(function () {
				var element = $(this);
				$(document).ajaxStart(function () {
					$("body").addClass("overlay-wrapper");
					$("body").append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
				}).ajaxStop(function () {
					$(this).unbind("ajaxStart");
					$("body").removeClass("overlay-wrapper");
					$("body").find("div.overlay").remove();
				}).ajaxError(function (e,xhr,opt) {
					console.log("ajax Error");
				});
			});
		}
	});

	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});

//		var baseURL = window.location.protocol + "//" + window.location.host + "/";
		var siteURL = window.location;
		var siteURLString = window.location+"";

		siteURL = siteURLString.split("install");
		$("#siteURL").val(siteURL[0]);
	});

	function showError(errorMessage,step){
		//Need To Display Error Message Here.
		var errorContent = $("#step"+step).find("p.error");
		errorContent.text(errorMessage);
		errorContent.show();
	}

	function hideErrorContents(){
		//Hide All the Error Div's
		var errorContent = $("p.error");
		errorContent.html('');
		errorContent.hide();
	}
	// for next step
	$('.next').click(function(){
		var nextId = $(this).parents('.tab-pane').next().attr("id");
		var currentID = $(this).parents('.tab-pane').attr("id");
		var returnData = canGoNext(currentID);
		if(returnData === false){
			return false;
		};

		$('body').myAjaxFunc();

		$.ajax({
			url:"setup.php",
			data: returnData,
			type:"POST",
			success:function(output){
				var data = output.split("::");
				if(data[0] === "FAIL"){
					showError(data[1]);
					return false;
				}else{
					hideErrorContents();
					$('[href=#'+nextId+']').tab('show');
				}
			}
		});

		return false;
	});

	$('.previous').click(function(){
		var previousId = $(this).parents('.tab-pane').prev().attr("id");
		$('[href=#'+previousId+']').tab('show');
		return false;
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		//update progress
		var step = $(e.target).data('step');
		var percent = (parseInt(step) / 3) * 100;

		$('.progress-bar').css({width: percent + '%'});
		$('.progress-bar').text("Step" + step + " of 3");
		//e.relatedTarget // previous tab
	});

	$('.first').click(function(){

		var currentID = $(this).parents('.tab-pane').attr("id");
		var returnData = canGoNext(currentID);
		if(returnData === false){
			return false;
		};


		$('body').myAjaxFunc();

		$.ajax({
			url:"setup.php",
			data: returnData,
			type:"POST",
			success:function(output){
				var data = output.split("::");
				if(data[0] === "FAIL"){
					showError(data[1],3);
					return false;
				}else{
					hideErrorContents();
					console.log();
				}
			}
		});

	});

	function canGoNext($currentTabID){

		var dbName = $("#dbName");
		var dbHost = $("#hostname");
		var dbUsername = $("#dbUsername");
		var dbPass = $("#dbPassword");

		if($currentTabID === 'step1'){
			//Check If Database Connection Works or Not.

			var foundError = false;

			if(dbHost.val().length === 0){
				dbHost.parents("div.form-group").addClass("has-error");
				foundError = true;
			}else{
				dbHost.parents("div.form-group").removeClass("has-error");
			}

			if(dbName.val().length === 0){
				dbName.parents("div.form-group").addClass("has-error");
				foundError = true;
			}else{
				dbName.parents("div.form-group").removeClass("has-error");
			}

			if(dbUsername.val().length === 0){
				dbUsername.parents("div.form-group").addClass("has-error");
				foundError = true;
			}else{
				dbUsername.parents("div.form-group").removeClass("has-error");
			}

			if(foundError === true){
				showError("Please Fill In the Required Fields.");
				return false;
			}

			//If Any Div's Are Visible Then Just Hide them.
			hideErrorContents();

			var data = {
				step:"dbSetup",
				dbName: dbName.val(),
				dbHost: dbHost.val(),
				dbPass: dbPass.val(),
				dbUsername: dbUsername.val()
			};

			return data;
		}
		else if($currentTabID === 'step2'){
			var HospitalAddress = $("#HospitalAddress");
			var HospitalName = $("#hospitalName");
			var SiteName = $("#siteName");
			var SiteURL = $("#siteURL");

			var foundHError = false;

			//Hospital Name Validation
			if(HospitalName.val().length === 0){
				HospitalName.parents("div.form-group").addClass("has-error");
				foundHError = true;
			}else{
				HospitalName.parents("div.form-group").removeClass("has-error");
			}

			//Site Name Validation
			if(SiteName.val().length === 0){
				SiteName.parents("div.form-group").addClass("has-error");
				foundHError = true;
			}else{
				SiteName.parents("div.form-group").removeClass("has-error");
			}

			//Site URL Validation
			if(!isValidURL(SiteURL.val())){
				SiteURL.parents("div.form-group").addClass("has-error");
				foundHError = true;
			}else{
				SiteURL.parents("div.form-group").removeClass("has-error");
			}

			if(foundHError === true){
				showError("Please Fill In the Required Fields.",2);
				return false;
			}

			//If Any Div's Are Visible Then Just Hide them.
			hideErrorContents();

			var hospitalData = {
				step:"hospitalSetup",
				HospitalName: HospitalName.val(),
				HospitalAddress: HospitalAddress.val(),
				SiteName: SiteName.val(),
				SiteURL:SiteURL.val(),

				//Connection Details For Connecting To DB.
				dbName: dbName.val(),
				dbHost: dbHost.val(),
				dbPass: dbPass.val(),
				dbUsername: dbUsername.val()
			};

			return hospitalData;
			return false;
		}
		else if($currentTabID === 'step3'){

			var username = $("#username");
			var email = $("#email");
			var password = $("#password");
			var confirmPass = $("#confirmPass");

			var foundUError = false;
			var foundErrorMessage = "";

			if(username.val().length < 3){
				username.parents("div.form-group").addClass("has-error");
				foundErrorMessage += "Username must consist of 3 or more characters";
				foundUError = true;
			}else{
				username.parents("div.form-group").removeClass("has-error");
			}

			if(!isValidEmailAddress(email.val())){
				email.parents("div.form-group").addClass("has-error");
				if(foundErrorMessage.length === 0){
					foundErrorMessage += "Invalid Email Address";
				}
				foundUError = true;
			}else{
				email.parents("div.form-group").removeClass("has-error");
			}

			if(password.val().length < 4){
				password.parents("div.form-group").addClass("has-error");
				if(foundErrorMessage.length === 0){
					foundErrorMessage += "Password can not be less than 4 characters";
				}
				foundUError = true;
			}else{
				password.parents("div.form-group").removeClass("has-error");
			}

			if(password.val() !== confirmPass.val()){
				confirmPass.parents("div.form-group").addClass("has-error");
				if(foundErrorMessage.length === 0){
					foundErrorMessage += "Passwords Do Not Match";
				}
				foundUError = true;
			}else{
				confirmPass.parents("div.form-group").removeClass("has-error");
			}

			console.log(foundErrorMessage);

			if(foundUError === true){
				console.log("working");
				showError(foundErrorMessage,3);
				return false;
			}

			//If Any Error Div's Are Visible Then Just Hide them.
			hideErrorContents();

			var userData = {
				step:"userSetup",
				username: username.val(),
				email: email.val(),
				password: password.val(),
				confirmPass: confirmPass.val(),

				//Connection Details For Connecting To DB.
				dbName: dbName.val(),
				dbHost: dbHost.val(),
				dbPass: dbPass.val(),
				dbUsername: dbUsername.val()
			};

			return userData;
		}
	}


	//Check if Email is Valid
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

	//Check if URL is Valid
	function isValidURL(url) {
		var pattern = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
		return pattern.test(url);
	}
</script>
</body>
</html>
