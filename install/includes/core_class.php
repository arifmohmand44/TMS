<?php

class Core {

	// Function to validate the post data
	function validate_post($data)
	{
		/* Validating the hostname, the database name and the username. The password is optional. */
		return !empty($data['dbHost']) && !empty($data['dbUsername']) && !empty($data['dbName']);
	}

	// Function to write the config file
	function write_config($data) {

		// Config path
		$template_path 	= 'config/database.php';
		$output_path 	= '../application/config/production/database.php';

		// Open the file
		$database_file = file_get_contents($template_path);

		$new  = str_replace("%HOSTNAME%",$data['dbHost'],$database_file);
		$new  = str_replace("%USERNAME%",$data['dbUsername'],$new);
		$new  = str_replace("%PASSWORD%",$data['dbPass'],$new);
		$new  = str_replace("%DATABASE%",$data['dbName'],$new);

		// Write the new database.php file
		$handle = fopen($output_path,'w+');

		// Chmod the file, in case the user forgot
		@chmod($output_path,0777);

		// Verify file permissions
		if(is_writable($output_path)) {

			// Write the file
			if(fwrite($handle,$new)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	function update_baseURL($data){
		// Config File path
		$template_path 	= 'config/config.php';
		$output_path 	= '../application/config/production/config.php';

		// Open the file
		$database_file = file_get_contents($template_path);
		$new  = str_replace("%BASEURL%",$data['SiteURL'],$database_file);

		// Write the new config.php file
		$handle = fopen($output_path,'w+');

		// Chmod the file, in case the user forgot
		@chmod($output_path,0777);

		// Verify file permissions
		if(is_writable($output_path)) {

			// Write the file
			if(fwrite($handle,$new)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}
}