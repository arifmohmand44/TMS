<?php

class Database {

	// Function to the database and tables and fill them with the default data
	function create_database($data)
	{

		mysqli_report(MYSQLI_REPORT_STRICT);

		try{
			// Connect to the database
			$mysqli = new mysqli($data['dbHost'],$data['dbUsername'],$data['dbPass'],'');

			// Check connection
			if ($mysqli->connect_error) {
				return false;
			}

			// Create the prepared statement
			$mysqli->query("CREATE DATABASE IF NOT EXISTS ".$data['dbName']);

			// Close the connection
			$mysqli->close();

		} catch (Exception $e ) {
			return false;
		}

		return true;
	}

	// Function to create the tables and fill them with the default data
	function create_tables($data)
	{
		try {
			// Connect to the database
			$mysqli = new mysqli($data['dbHost'], $data['dbUsername'], $data['dbPass'], $data['dbName']);

			// Check for errors
			if (mysqli_connect_errno())
				return false;

			// Open the default SQL file
//			$query = file_get_contents('assets/install.sql');
			$query = file_get_contents('assets/HMS.sql');

			$dbSelector = "Use `".$data['dbName']."`; ";
			$query = $dbSelector.$query;

			// Execute a multi query
			if($mysqli->multi_query($query)){
				do {
					// fetch results

					if (!$mysqli->more_results()) {
						break;
					}
					if (!$mysqli->next_result()) {
						if($mysqli->errno){
							if($mysqli->error === "Variable 'sql_mode' can't be set to the value of 'NULL'"){
								return true;
							}else{
								die("FAIL::".$mysqli->error."::error");
							}
						}
						break;
					}
				} while (true);
			}

			// Close the connection
			$mysqli->close();
		}
		catch(Exception $e){
			return false;
		}

		return true;
	}

	function insert_hospital_details($data){
		$hospitalFULLName = $data['HospitalName'];
		$SiteName = $data['SiteName'];
		$HospitalAddress = $data['HospitalAddress'];
		try{
			// Connect to the database
			$mysqli = new mysqli($data['dbHost'], $data['dbUsername'], $data['dbPass'], $data['dbName']);

			// Check for errors
			if (mysqli_connect_errno())
				return false;

			// Create the prepared statement
			$mysqli->query("UPDATE `sys_settings` SET VALUE ='".$SiteName."' WHERE Type='SiteName'");
			$mysqli->query("UPDATE `sys_settings` SET VALUE ='".$hospitalFULLName."' WHERE Type = 'HospitalFullName'");
			if(!empty($HospitalAddress)){
				$mysqli->query("UPDATE `sys_settings` SET VALUE ='".$HospitalAddress."' WHERE Type = 'HospitalAddress'");
			}

			// Close the connection
			$mysqli->close();

		} catch (Exception $e){
			return false;
		}
	}

	function insert_user_details($data){
		$username = $data['username'];
		$email = $data['email'];
		$password = trim($data['password']);

		try{
			// Connect to the database
			$mysqli = new mysqli($data['dbHost'], $data['dbUsername'], $data['dbPass'], $data['dbName']);

			// Check for errors
			if (mysqli_connect_errno())
				return false;

			// Create the prepared statement
			$query = "INSERT INTO `um_users` (`GroupID`, `Username`, `Email`, `Password`, `IsActive`, `DateCreated`, `CreatedBy`) VALUES( 1, '".$username."', '".$email."', '".$password."', 1, '".date('Y-m-d H:i:s')."', 1)";

			$mysqli->query($query);
			$lastInsertedID = $mysqli->insert_id;

			if($lastInsertedID === 0){
				echo "FAIL::New Record Could Not Be Created::error";
				echo $mysqli->error;
				return false;
			}else if($lastInsertedID > 0){
				// Close the connection
				$mysqli->close();
				return true;
			}
		} catch (Exception $e){
			return false;
		}
	}
}