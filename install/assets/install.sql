



SET FOREIGN_KEY_CHECKS=0;

/*Table structure for table `hms_acc_transactions` */

DROP TABLE IF EXISTS `hms_acc_transactions`;

CREATE TABLE `hms_acc_transactions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AccountID` int(10) NOT NULL,
  `TypeID` tinyint(1) NOT NULL,
  `Description` text COLLATE utf8_bin,
  `Amount` decimal(10,0) NOT NULL,
  `Dated` datetime NOT NULL,
  `TransactedBy` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_accountant` */

DROP TABLE IF EXISTS `hms_accountant`;

CREATE TABLE `hms_accountant` (
  `AccountantID` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(10) NOT NULL,
  `FullName` varchar(60) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`AccountantID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `hms_administrator` */

DROP TABLE IF EXISTS `hms_administrator`;

CREATE TABLE `hms_administrator` (
  `ID` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(10) unsigned NOT NULL,
  `FullName` varchar(100) COLLATE utf8_bin NOT NULL,
  `ContactNo` varchar(14) COLLATE utf8_bin DEFAULT NULL,
  `Address` text COLLATE utf8_bin,
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `DisplayPicture` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_UserID` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_ward` */

DROP TABLE IF EXISTS `ml_ward`;

CREATE TABLE `ml_ward` (
  `ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Identification` varchar(20) COLLATE utf8_bin NOT NULL,
  `Description` text COLLATE utf8_bin,
  `Date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Date_Updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Column For Trash',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_beds` */

DROP TABLE IF EXISTS `ml_beds`;

CREATE TABLE `ml_beds` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WardID` int(3) unsigned NOT NULL,
  `Identification` varchar(80) COLLATE utf8_bin NOT NULL,
  `IsAvailable` tinyint(1) NOT NULL DEFAULT '1',
  `Rent` decimal(10,2) DEFAULT NULL COMMENT 'Per Day Rent',
  `Description` text COLLATE utf8_bin,
  `Date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Date_Updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `WardID` (`WardID`),
  CONSTRAINT `ml_beds_ibfk_1` FOREIGN KEY (`WardID`) REFERENCES `ml_ward` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_room_class` */

DROP TABLE IF EXISTS `ml_room_class`;

CREATE TABLE `ml_room_class` (
  `ID` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) COLLATE utf8_bin NOT NULL,
  `Description` text COLLATE utf8_bin,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` tinyint(1) NOT NULL,
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_rooms` */

DROP TABLE IF EXISTS `ml_rooms`;

CREATE TABLE `ml_rooms` (
  `ID` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `ClassID` int(2) unsigned NOT NULL,
  `Identification` varchar(80) COLLATE utf8_bin NOT NULL COMMENT 'Name or Number',
  `IsAvailable` tinyint(1) NOT NULL DEFAULT '1',
  `Description` text COLLATE utf8_bin,
  `Rent` decimal(10,0) NOT NULL DEFAULT '0' COMMENT 'Per Day Rent For Now',
  `Date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Date_Updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'For Trash Status',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'To Enable / Desable',
  PRIMARY KEY (`ID`),
  KEY `ClassID` (`ClassID`),
  CONSTRAINT `ml_rooms_ibfk_1` FOREIGN KEY (`ClassID`) REFERENCES `ml_room_class` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_admission_purpose_type` */

DROP TABLE IF EXISTS `ml_admission_purpose_type`;

CREATE TABLE `ml_admission_purpose_type` (
  `ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) NOT NULL,
  `Description` text,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `hms_admission` */

DROP TABLE IF EXISTS `hms_admission`;

CREATE TABLE `hms_admission` (
  `AdmissionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatID` int(10) unsigned NOT NULL,
  `RoomTypeID` tinyint(1) unsigned NOT NULL,
  `RoomID` int(6) unsigned DEFAULT NULL,
  `BedID` int(10) unsigned DEFAULT NULL,
  `TotalDays` int(11) DEFAULT NULL,
  `AdmissionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DischargeDate` datetime DEFAULT NULL,
  `AdmittedFor` int(3) unsigned NOT NULL,
  `ConsultantID` int(5) unsigned DEFAULT NULL,
  `DateTimeStamp` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `StatusID` tinyint(1) NOT NULL DEFAULT '1',
  `AdmittedBy` int(8) NOT NULL,
  `DischargedBy` int(8) DEFAULT NULL,
  PRIMARY KEY (`AdmissionID`),
  KEY `FK_RoomType` (`RoomTypeID`),
  KEY `FK_PatientID` (`PatID`),
  KEY `BedID` (`BedID`),
  KEY `RoomID` (`RoomID`),
  KEY `AdmittedFor` (`AdmittedFor`),
  CONSTRAINT `FK_RoomType` FOREIGN KEY (`RoomTypeID`) REFERENCES `ml_room_type` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `hms_admission_ibfk_1` FOREIGN KEY (`BedID`) REFERENCES `ml_beds` (`ID`),
  CONSTRAINT `hms_admission_ibfk_2` FOREIGN KEY (`RoomID`) REFERENCES `ml_rooms` (`ID`),
  CONSTRAINT `hms_admission_ibfk_3` FOREIGN KEY (`AdmittedFor`) REFERENCES `ml_admission_purpose_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_doctor` */

DROP TABLE IF EXISTS `hms_doctor`;

CREATE TABLE `hms_doctor` (
  `ID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(60) NOT NULL,
  `Speciality` varchar(80) NOT NULL,
  `ContactNo` varchar(14) DEFAULT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `hms_invoice_header` */

DROP TABLE IF EXISTS `hms_invoice_header`;

CREATE TABLE `hms_invoice_header` (
  `InvoiceID` bigint(14) unsigned NOT NULL AUTO_INCREMENT,
  `PatID` int(10) unsigned NOT NULL,
  `AdmissionID` tinyint(10) DEFAULT NULL,
  `Discount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `Total` decimal(20,2) NOT NULL,
  `InvoiceDate` datetime NOT NULL,
  `InvoiceStatus` tinyint(1) NOT NULL,
  `IsPrinted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'To Know if Inovice Was Printed',
  `VatTaxPercentage` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `VatTaxCalculatedAmount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`InvoiceID`),
  KEY `FK_Invoice_PatientID` (`PatID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_invoice_lines` */

DROP TABLE IF EXISTS `hms_invoice_lines`;

CREATE TABLE `hms_invoice_lines` (
  `InvoiceLineID` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `InvoiceID` bigint(14) unsigned NOT NULL,
  `PaymentID` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `subTotal` decimal(10,0) NOT NULL,
  `Quantity` int(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`InvoiceLineID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_ot_procedures` */

DROP TABLE IF EXISTS `hms_ot_procedures`;

CREATE TABLE `hms_ot_procedures` (
  `ProcedureID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProcedureTypeID` int(3) unsigned DEFAULT NULL,
  `PatID` int(10) unsigned NOT NULL,
  `OT_ID` int(3) unsigned NOT NULL,
  `ConsultantID` int(5) unsigned NOT NULL,
  `StartDate` date NOT NULL,
  `StartTime` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '00:00 AM',
  `EndDate` date DEFAULT NULL,
  `EndTime` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '00:00 AM',
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ProcedureStatusID` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ProcedureID`),
  KEY `hms_ot_procedures_ibfk_1` (`PatID`),
  KEY `ProcedureTypeID` (`ProcedureTypeID`),
  KEY `OT_ID` (`OT_ID`),
  CONSTRAINT `hms_ot_procedures_ibfk_2` FOREIGN KEY (`ProcedureTypeID`) REFERENCES `ml_procedure_types` (`ID`),
  CONSTRAINT `hms_ot_procedures_ibfk_3` FOREIGN KEY (`OT_ID`) REFERENCES `ml_ot` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_patient` */

DROP TABLE IF EXISTS `hms_patient`;

CREATE TABLE `hms_patient` (
  `P_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `P_RefNo` varchar(10) COLLATE utf8_bin NOT NULL,
  `BloodGroupID` int(2) unsigned DEFAULT NULL,
  `Full_Name` varchar(80) COLLATE utf8_bin NOT NULL,
  `Father_Name` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `GenderID` tinyint(1) DEFAULT NULL,
  `Age` int(3) NOT NULL,
  `Address` text COLLATE utf8_bin,
  `CNIC` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `Emergency_Contact` varchar(14) COLLATE utf8_bin DEFAULT NULL,
  `PatientTypeID` int(11) NOT NULL DEFAULT '1',
  `Date_Added` datetime NOT NULL,
  `AddedBy` int(10) NOT NULL,
  `Date_Updated` datetime DEFAULT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`P_ID`),
  KEY `BloodGroupID` (`BloodGroupID`),
  CONSTRAINT `hms_patient_ibfk_1` FOREIGN KEY (`BloodGroupID`) REFERENCES `ml_bloodgroup` (`BloodGroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_patient_account` */

DROP TABLE IF EXISTS `hms_patient_account`;

CREATE TABLE `hms_patient_account` (
  `AccountID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatientID` int(10) unsigned NOT NULL,
  `CurrentBalance` decimal(10,0) NOT NULL DEFAULT '0',
  `RefundAmount` decimal(10,0) NOT NULL DEFAULT '0',
  `DateAdded` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`AccountID`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_payments` */

DROP TABLE IF EXISTS `hms_payments`;

CREATE TABLE `hms_payments` (
  `PaymentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PHeadID` int(6) unsigned NOT NULL,
  `PatID` int(10) unsigned NOT NULL,
  `AdmissionID` int(10) DEFAULT NULL,
  `Amount` decimal(20,2) unsigned NOT NULL DEFAULT '0.00',
  `PaymentDate` datetime NOT NULL,
  `ReceivedBy` int(8) unsigned DEFAULT NULL,
  `ReceivedDate` datetime DEFAULT NULL,
  `Status` tinyint(1) NOT NULL,
  PRIMARY KEY (`PaymentID`),
  KEY `PatID` (`PatID`),
  KEY `PHeadID` (`PHeadID`),
  CONSTRAINT `hms_payments_ibfk_2` FOREIGN KEY (`PHeadID`) REFERENCES `ml_payment_heads` (`PHeadID`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hms_product` */

DROP TABLE IF EXISTS `hms_product`;

CREATE TABLE `hms_product` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `Product_Code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Alpha numeric code that will be entered by the pharmacist',
  `Product_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Product name',
  `Bar_Code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Formulation` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Batch_No` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Purchase_Price` decimal(10,0) NOT NULL,
  `Sale_Price` decimal(10,0) NOT NULL,
  `Available_Qty` int(11) NOT NULL COMMENT 'Quantity currently present in stock',
  `Reorder_Level` int(11) NOT NULL,
  `Category_ID` int(11) NOT NULL,
  `Supplier_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Manufacture` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Product_Location` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UnitID` int(11) NOT NULL,
  `Product_Status` tinyint(1) NOT NULL DEFAULT '1',
  `On_Order_Status` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `hms_supplier` */

DROP TABLE IF EXISTS `hms_supplier`;

CREATE TABLE `hms_supplier` (
  `Supplier_ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Supplier_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Contact_person_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` text COLLATE utf8_unicode_ci,
  `Phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Supplier_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_active` */

DROP TABLE IF EXISTS `ml_active`;

CREATE TABLE `ml_active` (
  `ID` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `ml_admission_purpose_type` */

DROP TABLE IF EXISTS `ml_admission_purpose_type`;

CREATE TABLE `ml_admission_purpose_type` (
  `ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) NOT NULL,
  `Description` text,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `ml_admission_status` */

DROP TABLE IF EXISTS `ml_admission_status`;

CREATE TABLE `ml_admission_status` (
  `ID` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_availability_status` */

DROP TABLE IF EXISTS `ml_availability_status`;

CREATE TABLE `ml_availability_status` (
  `ID` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `Status` varchar(30) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_bloodgroup` */

DROP TABLE IF EXISTS `ml_bloodgroup`;

CREATE TABLE `ml_bloodgroup` (
  `BloodGroupID` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BloodGroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_category` */

DROP TABLE IF EXISTS `ml_category`;

CREATE TABLE `ml_category` (
  `Cat_ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `Category_Name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Cat_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_currency` */

DROP TABLE IF EXISTS `ml_currency`;

CREATE TABLE `ml_currency` (
  `ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Currency` varchar(30) COLLATE utf8_bin NOT NULL,
  `symbol` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `FountAwesomeClass` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_gender` */

DROP TABLE IF EXISTS `ml_gender`;

CREATE TABLE `ml_gender` (
  `ID` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(10) COLLATE utf8_bin NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_ot` */

DROP TABLE IF EXISTS `ml_ot`;

CREATE TABLE `ml_ot` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Identification` varchar(80) COLLATE utf8_bin NOT NULL,
  `Description` text COLLATE utf8_bin,
  `Date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Date_Updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Operation Theatre';

/*Table structure for table `ml_patient_type` */

DROP TABLE IF EXISTS `ml_patient_type`;

CREATE TABLE `ml_patient_type` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `PatientType` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Patient Type Title',
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Is Deleted or not',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Enabled/Desabled',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_payment_heads` */

DROP TABLE IF EXISTS `ml_payment_heads`;

CREATE TABLE `ml_payment_heads` (
  `PHeadID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`PHeadID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_payment_status` */

DROP TABLE IF EXISTS `ml_payment_status`;

CREATE TABLE `ml_payment_status` (
  `StatusID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsEnabled` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_procedure_status` */

DROP TABLE IF EXISTS `ml_procedure_status`;

CREATE TABLE `ml_procedure_status` (
  `ID` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_procedure_types` */

DROP TABLE IF EXISTS `ml_procedure_types`;

CREATE TABLE `ml_procedure_types` (
  `ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) COLLATE utf8_bin NOT NULL,
  `Fee` decimal(12,2) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `ProcedureCategoryID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_procedures` */

DROP TABLE IF EXISTS `ml_procedures`;

CREATE TABLE `ml_procedures` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `procedure` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `IsEnabled` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


/*Table structure for table `ml_room_type` */

DROP TABLE IF EXISTS `ml_room_type`;

CREATE TABLE `ml_room_type` (
  `ID` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(80) COLLATE utf8_bin NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


/*Table structure for table `ml_service_type` */

DROP TABLE IF EXISTS `ml_service_type`;

CREATE TABLE `ml_service_type` (
  `ServiceID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(60) COLLATE utf8_bin NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ServiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_test_category` */

DROP TABLE IF EXISTS `ml_test_category`;

CREATE TABLE `ml_test_category` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CategoryTitle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `IsEnabled` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_test_status` */

DROP TABLE IF EXISTS `ml_test_status`;

CREATE TABLE `ml_test_status` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StatusTitle` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_tests` */

DROP TABLE IF EXISTS `ml_tests`;

CREATE TABLE `ml_tests` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TestCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TestName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Abbreviation` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ReferenceRange` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TestCategoryID` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsEnabled` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ml_transaction_type` */

DROP TABLE IF EXISTS `ml_transaction_type`;

CREATE TABLE `ml_transaction_type` (
  `ID` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(16) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_units` */

DROP TABLE IF EXISTS `ml_units`;

CREATE TABLE `ml_units` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Unit_Title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateUpdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `IsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



/*Table structure for table `patient_test_lines` */

DROP TABLE IF EXISTS `patient_test_lines`;

CREATE TABLE `patient_test_lines` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatientTestID` int(11) NOT NULL,
  `TestID` int(11) NOT NULL,
  `Result` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Remarks` text COLLATE utf8_unicode_ci,
  `Test_Price` decimal(10,0) NOT NULL DEFAULT '0',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `patient_tests` */

DROP TABLE IF EXISTS `patient_tests`;

CREATE TABLE `patient_tests` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LabRefNo` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `PatientID` int(11) DEFAULT NULL,
  `PatientTypeID` int(11) NOT NULL,
  `ConsultantID` int(11) NOT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `GenderID` int(11) DEFAULT NULL,
  `ContactNo` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalAmount` decimal(10,0) NOT NULL DEFAULT '0',
  `Discount` decimal(10,0) NOT NULL DEFAULT '0',
  `Tax` decimal(10,0) NOT NULL DEFAULT '0',
  `TotalPaid` decimal(10,0) NOT NULL DEFAULT '0',
  `Test_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ResDelivery date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateAdded` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TestAddedBy` int(11) NOT NULL,
  `TestStatus` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `patientrx` */

DROP TABLE IF EXISTS `patientrx`;

CREATE TABLE `patientrx` (
  `PatientRx_ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `Prescription_Ref` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Patient_type` int(11) NOT NULL,
  `PatientID` int(11) DEFAULT NULL,
  `Patient_Name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `GenderID` int(11) DEFAULT NULL,
  `ContactNo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Total_Amount` decimal(10,0) NOT NULL DEFAULT '0',
  `Discount` decimal(10,0) NOT NULL DEFAULT '0',
  `Tax` decimal(10,0) NOT NULL DEFAULT '0',
  `Total_Paid` decimal(10,0) NOT NULL DEFAULT '0',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PatientRx_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `patientrx_lines` */

DROP TABLE IF EXISTS `patientrx_lines`;

CREATE TABLE `patientrx_lines` (
  `PatientRx_Line_ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `patientRx_ID` int(11) NOT NULL,
  `Product_ID` int(11) NOT NULL,
  `Quantity_Sold` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Sale_Price` decimal(10,0) NOT NULL DEFAULT '0',
  `PatRx_Line_Total` decimal(10,0) NOT NULL DEFAULT '0',
  `Sale_Date` datetime NOT NULL,
  `Quantity_Returned` int(11) NOT NULL DEFAULT '0',
  `Returned_Date` datetime DEFAULT NULL,
  `Line_total_price` decimal(10,0) NOT NULL DEFAULT '0',
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PatientRx_Line_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `product_re_order_status` */

DROP TABLE IF EXISTS `product_re_order_status`;

CREATE TABLE `product_re_order_status` (
  `ReorderID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Product_ID` int(11) NOT NULL,
  `Ordered_Quantity` int(11) NOT NULL,
  `Date_Ordered` datetime DEFAULT NULL,
  `Supplier_ID` int(11) NOT NULL,
  PRIMARY KEY (`ReorderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `sys_menus` */

DROP TABLE IF EXISTS `sys_menus`;

CREATE TABLE `sys_menus` (
  `MenuID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `ParentID` int(5) unsigned NOT NULL DEFAULT '0',
  `Title` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Tile That will be Displayed in Menu',
  `Icon` varchar(100) COLLATE utf8_bin NOT NULL,
  `CI_Path` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`MenuID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `sys_settings` */

DROP TABLE IF EXISTS `sys_settings`;

CREATE TABLE `sys_settings` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `Type` varchar(80) COLLATE utf8_bin NOT NULL,
  `Value` text COLLATE utf8_bin,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `um_groups_menus` */

DROP TABLE IF EXISTS `um_groups_menus`;

CREATE TABLE `um_groups_menus` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GroupID` int(8) DEFAULT NULL,
  `MenuID` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='um = user management\r\ndisplay menus according to groups';

/*Table structure for table `um_users` */

DROP TABLE IF EXISTS `um_users`;

CREATE TABLE `um_users` (
  `UserID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `GroupID` int(3) unsigned NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreatedBy` int(8) NOT NULL,
  `UpdatedBy` int(8) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `UNIQUE_Email` (`Email`),
  UNIQUE KEY `UNIQUE_Username` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `um_users_groups` */

DROP TABLE IF EXISTS `um_users_groups`;

CREATE TABLE `um_users_groups` (
  `GroupID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) COLLATE utf8_bin NOT NULL,
  `Description` text COLLATE utf8_bin,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `DateCreated` datetime NOT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` int(8) NOT NULL,
  `UpdatedBy` int(8) DEFAULT NULL,
  PRIMARY KEY (`GroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

SET FOREIGN_KEY_CHECKS=1;
